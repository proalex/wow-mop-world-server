package ru.ngwow.worldserver.database.query;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.exceptions.ConfigFileException;
import ru.ngwow.worldserver.settings.WorldSettings;

public class DBHelper {
    
    private static final Logger log = Logger.getLogger(DBHelper.class.getName());
    
    private DBHelper() {
    }
    
    public static void updateOnline() throws IOException, ConfigFileException, Exception {
        log.log(Level.INFO, "Cleaning online state for accounts...");
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.cleanOnline);
            statment.setInt(1, WorldSettings.getInt("world.id"));
            statment.execute();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
    }
}
