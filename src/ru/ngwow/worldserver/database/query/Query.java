package ru.ngwow.worldserver.database.query;

public interface Query {

    static String getBannedState = "SELECT COUNT(id) FROM account_banned WHERE "
            + "id = ? AND (unbandate > UNIX_TIMESTAMP() OR unbandate = 0)";
    static String getAccountInfo = "SELECT id, sessionkey, online, expansion FROM account WHERE email = ?";
    static String setOnlineState = "UPDATE account SET online = ? WHERE id = ?";
    static String getClassData = "SELECT * FROM class_expansion";
    static String getRaceData = "SELECT * FROM race_expansion";
    static String cleanOnline = "UPDATE account SET online = 0 WHERE online = ?";
    static String getCharacters = "SELECT * FROM characters WHERE accountid = ?";
    static String checkCharName = "SELECT * from characters WHERE name = ?";
    static String getCharacterCreationData = "SELECT map, zone, posX, posY, posZ,"
            + "posO FROM character_creation_data WHERE race = ? AND class = ?";
    static String createCharacter = "INSERT INTO characters (name, accountid,"
            + "race, class, gender, skin, zone, map, x, y, z, o, face, hairstyle,"
            + "haircolor, facialhair, characterFlags) VALUES (" +
            "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    static String incCharactersCount = "INSERT INTO characters (realmid, accountid, count) VALUES (?, ?, 1) ON DUPLICATE KEY UPDATE count = count + 1";
    static String decCharactersCount = "UPDATE characters SET count = count - 1 WHERE realmid = ? AND accountid = ?";
    static String deleteCharacter = "DELETE FROM characters WHERE guid = ? AND accountid = ?";
    static String getCharactersCount = "SELECT * FROM characters WHERE realmid = ? AND accountid = ?";
    static String getCharacterData = "SELECT * FROM characters WHERE guid = ?";
    static String deleteCharacterTalents = "DELETE FROM character_talents WHERE guid = ?";
    static String getCharacterTalents = "SELECT * FROM character_talents WHERE guid = ? ORDER BY specGroup ASC";
    static String getCharacterSkills = "SELECT * FROM character_skills WHERE guid = ? ORDER BY skill ASC";
    static String getCharacterCreationSkills = "SELECT skill FROM character_creation_skills WHERE race = ? ORDER BY skill ASC";
    static String addSkill = "REPLACE INTO character_skills (guid, skill) VALUES (?, ?)";
    static String updateCinematic = "UPDATE characters SET loginCinematic = 0 WHERE guid = ?";
    static String getCharacterSpells = "SELECT * FROM character_spells WHERE guid = ? ORDER BY spellId ASC";
    static String getCharacterCreationSpells = "SELECT spellId FROM character_creation_spells WHERE race = ? AND class = ? ORDER BY spellId ASC";
    static String addSpell = "INSERT INTO character_spells (guid, spellId) VALUES (?, ?)";
    static String getCharacterActions = "SELECT * FROM character_actions WHERE guid = ? ORDER BY slotId ASC";
    static String getCharacterCreationActions = "SELECT action, slotId FROM character_creation_actions WHERE race = ? AND class = ? ORDER BY slotId ASC";
    static String addActionButton = "INSERT INTO character_actions (guid, action, slotId, specGroup) VALUES (?, ?, ?, ?)";
    static String deleteActionButton = "DELETE FROM character_actions WHERE guid = ? AND action = ? AND slotId = ? AND specGroup = ?";
    static String getRealmName = "SELECT name FROM realms WHERE id = ?";
    static String setPlayerZone = "UPDATE characters SET zone = ? WHERE guid = ?";
    static String setPlayerPosition = "UPDATE characters SET x = ?, y = ?, z = ?, o = ? WHERE guid = ?";
    static String getCreatureStats = "SELECT * FROM creature_stats WHERE id = ?";
    static String getCreatureData = "SELECT * FROM creature_data WHERE id = ?";
    static String getCreatureSpawns = "SELECT * FROM creature_spawns";
}
