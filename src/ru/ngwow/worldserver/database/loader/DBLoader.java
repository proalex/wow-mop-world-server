package ru.ngwow.worldserver.database.loader;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.data.ClassExpansion;
import ru.ngwow.worldserver.database.data.RaceExpansion;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.managers.SpawnMgr;

public class DBLoader {

    private static ArrayList<ClassExpansion> classData = new ArrayList<>();
    private static ArrayList<RaceExpansion> raceData = new ArrayList<>();
    private static final Logger log = Logger.getLogger(DBLoader.class.getName());
    
    private DBLoader() {
    }
    
    public static void loadClassData() throws SQLException, Exception {
        log.log(Level.INFO, "Loading class_expansion table data...");
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getClassData);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        classData.clear();
        while (result.next()) {
            classData.add(new ClassExpansion(
                    result.getShort("class"), 
                    result.getShort("expansion")));
        }
    }
    
    public static void loadRaceData() throws SQLException, Exception {
        log.log(Level.INFO, "Loading race_expansion table data...");
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getRaceData);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        raceData.clear();
        while (result.next()) {
            raceData.add(new RaceExpansion(
                result.getShort("race"),
                result.getShort("expansion")));
        }
    }
    
    public static void loadCreatureData() throws Exception {
        log.log(Level.INFO, "Loading creature_spawns table data...");
        SpawnMgr.loadCreatureSpawns();
    }
    
    public static void loadData() throws  Exception {
        log.log(Level.INFO, "Loading database data...");
        loadClassData();
        loadRaceData();
        loadCreatureData();
    }
    
    public static ArrayList getClassData() {
        return classData;
    }
    
    public static ArrayList getRaceData() {
        return raceData;
    }
}
