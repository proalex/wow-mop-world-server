package ru.ngwow.worldserver.database;

import java.io.IOException;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.commons.pool.impl.GenericObjectPoolFactory;
import ru.ngwow.worldserver.exceptions.ConfigFileException;
import ru.ngwow.worldserver.settings.WorldSettings;

public class MySqlConnection {

    private static volatile MySqlConnection instance;
    private static ObjectPool authPool;
    private static ObjectPool charPool;
    private static ObjectPool worldPool;

    private MySqlConnection() throws IOException, ConfigFileException {
        String authHost = WorldSettings.get("mysql.auth.host");
        int authPort = new Integer(WorldSettings.get("mysql.auth.port"));
        String authSchema = WorldSettings.get("mysql.auth.schema");
        String authUser = WorldSettings.get("mysql.auth.user");
        String authPassword = WorldSettings.get("mysql.auth.password");
        int authThreads = WorldSettings.getInt("mysql.auth.threads");
        PoolableObjectFactory authFactory = new MySqlPoolableObjectFactory(authHost,
                authPort, authSchema, authUser, authPassword);
        GenericObjectPool.Config authPoolConfig = new GenericObjectPool.Config();
        authPoolConfig.maxActive = authThreads;
        authPoolConfig.testOnBorrow = true;
        authPoolConfig.testWhileIdle = true;
        authPoolConfig.timeBetweenEvictionRunsMillis = 10000;
        authPoolConfig.minEvictableIdleTimeMillis = 60000;
        GenericObjectPoolFactory authObjectPoolFactory =
                new GenericObjectPoolFactory(authFactory, authPoolConfig);
        String charHost = WorldSettings.get("mysql.characters.host");
        int charPort = WorldSettings.getInt("mysql.characters.port");
        String charSchema = WorldSettings.get("mysql.characters.schema");
        String charUser = WorldSettings.get("mysql.characters.user");
        String charPassword = WorldSettings.get("mysql.characters.password");
        int charThreads = WorldSettings.getInt("mysql.characters.threads");
        PoolableObjectFactory charFactory = new MySqlPoolableObjectFactory(charHost,
                charPort, charSchema, charUser, charPassword);
        GenericObjectPool.Config charPoolConfig = new GenericObjectPool.Config();
        charPoolConfig.maxActive = charThreads;
        charPoolConfig.testOnBorrow = true;
        charPoolConfig.testWhileIdle = true;
        charPoolConfig.timeBetweenEvictionRunsMillis = 10000;
        charPoolConfig.minEvictableIdleTimeMillis = 60000;
        GenericObjectPoolFactory charObjectPoolFactory =
                new GenericObjectPoolFactory(charFactory, charPoolConfig);
        String worldHost = WorldSettings.get("mysql.world.host");
        int worldPort = WorldSettings.getInt("mysql.world.port");
        String worldSchema = WorldSettings.get("mysql.world.schema");
        String worldUser = WorldSettings.get("mysql.world.user");
        String worldPassword = WorldSettings.get("mysql.world.password");
        int worldThreads = WorldSettings.getInt("mysql.world.threads");
        PoolableObjectFactory worldFactory = new MySqlPoolableObjectFactory(worldHost,
                worldPort, worldSchema, worldUser, worldPassword);
        GenericObjectPool.Config worldPoolConfig = new GenericObjectPool.Config();
        worldPoolConfig.maxActive = worldThreads;
        worldPoolConfig.testOnBorrow = true;
        worldPoolConfig.testWhileIdle = true;
        worldPoolConfig.timeBetweenEvictionRunsMillis = 10000;
        worldPoolConfig.minEvictableIdleTimeMillis = 60000;
        GenericObjectPoolFactory worldObjectPoolFactory =
                new GenericObjectPoolFactory(worldFactory, worldPoolConfig);
        authPool = authObjectPoolFactory.createPool();
        charPool = charObjectPoolFactory.createPool();
        worldPool = worldObjectPoolFactory.createPool();
    }

    public static MySqlConnection getInstance()
            throws IOException, ConfigFileException {
        MySqlConnection localInstance = instance;
        if (localInstance == null) {
            synchronized (MySqlConnection.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new MySqlConnection();
                }
            }
        }
        return localInstance;
    }

    public ObjectPool getAuthPool() {
        return authPool;
    }

    public ObjectPool getCharPool() {
        return charPool;
    }

    public ObjectPool getWorldPool() {
        return worldPool;
    }
}
