package ru.ngwow.worldserver.database.data;

public class ClassExpansion {
    
    private short classid;
    private short expansion;
    
    public ClassExpansion(short classid, short expansion) {
        this.classid = classid;
        this.expansion = expansion;
    }
    
    public int getClassId() {
        return classid;
    }
    
    public int getExpansion() {
        return expansion;
    }
}
