package ru.ngwow.worldserver.database.data;

public class CharacterData {
    
    private long guid;
    private int accountid;
    private String name;
    private int race;
    private int classid;
    private int gender;
    private int skin;
    private int face;
    private int hairStyle;
    private int hairColor;
    private int facialHair;
    private int level;
    private int zone;
    private int map;
    private float x;
    private float y;
    private float z;
    private float o;
    private long guildGuid;
    private int petDisplayId;
    private int petLevel;
    private int petFamily;
    private int characterFlags;
    private int customizeFlags;
    private boolean loginCinematic;
    private int specGroupCount;
    private int activeSpecGroup;
    private int primarySpecId;
    private int secondarySpecId;
    
    public CharacterData(long guid, int accountid, String name, int race, int classid, int gender,
            int skin, int face, int hairStyle, int hairColor, int facialHair, int level,
            int zone, int map, float x, float y, float z, float o, long guildGuid,
            int petDisplayId, int petLevel, int petFamily, int characterFlags, int customizeFlags,
            boolean loginCinematic, int specGroupCount, int activeSpecGroup, int primarySpecId,
            int secondarySpecId) {
        this.guid = guid;
        this.accountid = accountid;
        this.name = name;
        this.race = race;
        this.classid = classid;
        this.gender = gender;
        this.skin = skin;
        this.face = face;
        this.hairStyle = hairStyle;
        this.hairColor = hairColor;
        this.facialHair = facialHair;
        this.level = level;
        this.zone = zone;
        this.map = map;
        this.x = x;
        this.y = y;
        this.z = z;
        this.o = o;
        this.guildGuid = guildGuid;
        this.petDisplayId = petDisplayId;
        this.petLevel = petLevel;
        this.petFamily = petFamily;
        this.characterFlags = characterFlags;
        this.customizeFlags = customizeFlags;
        this.specGroupCount = specGroupCount;
        this.activeSpecGroup = activeSpecGroup;
        this.primarySpecId = primarySpecId;
        this.secondarySpecId = secondarySpecId;
        this.loginCinematic = loginCinematic;
    }

    public long getGuid() {
        return guid;
    }

    public int getAccountid() {
        return accountid;
    }

    public String getName() {
        return name;
    }

    public int getRace() {
        return race;
    }

    public int getClassId() {
        return classid;
    }

    public int getGender() {
        return gender;
    }

    public int getSkin() {
        return skin;
    }

    public int getFace() {
        return face;
    }

    public int getHairStyle() {
        return hairStyle;
    }

    public int getHairColor() {
        return hairColor;
    }

    public int getFacialHair() {
        return facialHair;
    }

    public int getLevel() {
        return level;
    }

    public int getZone() {
        return zone;
    }

    public int getMap() {
        return map;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

    public float getO() {
        return o;
    }

    public long getGuildGuid() {
        return guildGuid;
    }

    public int getPetDisplayId() {
        return petDisplayId;
    }

    public int getPetLevel() {
        return petLevel;
    }

    public int getPetFamily() {
        return petFamily;
    }

    public int getCharacterFlags() {
        return characterFlags;
    }

    public int getCustomizeFlags() {
        return customizeFlags;
    }

    public boolean isLoginCinematic() {
        return loginCinematic;
    }

    public int getSpecGroupCount() {
        return specGroupCount;
    }

    public int getActiveSpecGroup() {
        return activeSpecGroup;
    }

    public int getPrimarySpecId() {
        return primarySpecId;
    }

    public int getSecondarySpecId() {
        return secondarySpecId;
    }
}
