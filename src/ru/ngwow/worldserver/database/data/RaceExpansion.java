package ru.ngwow.worldserver.database.data;

public class RaceExpansion {
    
    private short race;
    private short expansion;
    
    public RaceExpansion(short race, short expansion) {
        this.race = race;
        this.expansion = expansion;
    }
    
    public int getRace() {
        return race;
    }
    
    public int getExpansion() {
        return expansion;
    }
}