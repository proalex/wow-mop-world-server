package ru.ngwow.worldserver.constants;

public enum ContainerDynamicArrays {
    
    END(ItemDynamicArrays.END.getData());
    
    private int data;
    
    ContainerDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
