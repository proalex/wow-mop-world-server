package ru.ngwow.worldserver.constants;

import ru.ngwow.worldserver.exceptions.NoSuchFlagException;

public enum MovementFlag {
    
    FORWARD(0x1),
    BACKWARD(0x2),
    STRAFE_LEFT(0x4),
    STRAFE_RIGHT(0x8),
    TURN_LEFT(0x10),
    TURN_RIGHT(0x20),
    PITCH_UP(0x40),
    PITCH_DOWN(0x80),
    RUN_MODE(0x100),
    GRAVITY(0x200),
    ROOT(0x400),
    FALLING(0x800),
    FALL_RESET(0x1000),
    PENDING_STOP(0x2000),
    PENDING_STRAFE_STOP(0x4000),
    PENDING_FORWARD(0x8000),
    PENDING_BACKWARD(0x10000),
    PENDING_STRAFE_LEFT(0x20000),
    PENDING_STRAFE_RIGHT(0x40000),
    PENDING_ROOT(0x80000),
    SWIM(0x100000),
    ASCENSION(0x200000),
    DESCENSION(0x400000),
    CAN_FLY(0x800000),
    FLIGHT(0x1000000),
    JUMP(0x2000000),
    WALK_ON_WATER(0x4000000),
    FEATHER_FALL(0x8000000),
    HOVER_MOVE(0x10000000),
    COLLISION(0x20000000);
    
    private int id;
    
    MovementFlag(int id) {
        this.id = id;
    }
    
    public int getData() {
        return id;
    }
    
    public static MovementFlag getFlag(int data) throws NoSuchFlagException {
        for (MovementFlag current : MovementFlag.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchFlagException();
    }
}
