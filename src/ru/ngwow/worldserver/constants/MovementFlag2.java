package ru.ngwow.worldserver.constants;

import ru.ngwow.worldserver.exceptions.NoSuchFlagException;

public enum MovementFlag2 {
    
    UNKNOWN(0x1),
    UNKNOWN2(0x2),
    UNKNOWN3(0x4),
    UNKNOWN4(0x8),
    UNKNOWN5(0x10),
    UNKNOWN6(0x20),
    UNKNOWN7(0x40),
    UNKNOWN8(0x80),
    UNKNOWN9(0x100),
    UNKNOWN10(0x200),
    UNKNOWN11(0x400),
    UNKNOWN12(0x2000),
    UNKNOWN13(0x4000);
    
    private int id;
    
    MovementFlag2(int id) {
        this.id = id;
    }
    
    public int getData() {
        return id;
    }
    
    public static MovementFlag2 getFlag(int data) throws NoSuchFlagException {
        for (MovementFlag2 current : MovementFlag2.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchFlagException();
    }
}
