package ru.ngwow.worldserver.constants;

import ru.ngwow.worldserver.exceptions.NoSuchGuidTypeException;

public enum HighGuidType {
    
    PLAYER(0x000),
    GROUP(0x1F5),
    MOTRANSPORT(0x1FC),
    GUILD(0x10F),
    BATTLEFIELD(0x1F1),
    INSTANCE(0x1F4),
    ITEM(0x440),
    CONTAINER(0x440),
    DYNAMICOBJECT(0xF10),
    CORPSE(0xF50),
    GAMEOBJECT(0xF11),
    TRANSPORT(0xF12),
    UNIT(0xF13),
    PET(0xF14),
    VEHICLE(0xF15);
    
    private int data;
    
    HighGuidType(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
    
    public static HighGuidType getGuidType(long data) throws NoSuchGuidTypeException {
        for (HighGuidType current : HighGuidType.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchGuidTypeException();
    }
}
