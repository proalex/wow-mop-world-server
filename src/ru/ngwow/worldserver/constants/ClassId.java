package ru.ngwow.worldserver.constants;

public enum ClassId {
    
    WARRIOR(1),
    PALADIN(2),
    HUNTER(3),
    ROGUE(4),
    PRIEST(5),
    DEATHKNIGHT(6),
    SHAMAN(7),
    MAGE(8),
    WARLOCK(9),
    MONK(10),
    DRUID(11);
    
    public int data;
    
    ClassId(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
