package ru.ngwow.worldserver.constants;

public enum SceneObjectFields {
    
    SCRIPT_PACKAGE_ID(0x0),
    RND_SEED_VAL(0x1),
    CREATED_BY(0x2),
    END(0x4);
    
    private int data;
    
    SceneObjectFields(int data) {
        this.data = ObjectFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
