package ru.ngwow.worldserver.constants;

public enum AccountDataMasks {
    
    GLOBAL_CACHE_MASK(0x15),
    CHARACTER_CACHE_MASK(0xAA);
    
    private int data;
    
    AccountDataMasks(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}