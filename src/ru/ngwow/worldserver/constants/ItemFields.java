package ru.ngwow.worldserver.constants;

public enum ItemFields {
    
    OWNER(0x0),
    CONTAINED_IN(0x2),
    CREATOR(0x4),
    GIFT_CREATOR(0x6),
    STACK_COUNT(0x8),
    EXPIRATION(0x9),
    SPELL_CHARGES(0xA),
    DYNAMIC_FLAGS(0xF),
    ENCHANTMENT(0x10),
    PROPERTY_SEED(0x37),
    RANDOM_PROPERTIES_ID(0x38),
    DURABILITY(0x39),
    MAX_DURABILITY(0x3A),
    CREATE_PLAYED_TIME(0x3B),
    MODIFIERS_MASK(0x3C),
    END(0x3D);   
    
    private int data;
    
    ItemFields(int data) {
        this.data = ObjectFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
