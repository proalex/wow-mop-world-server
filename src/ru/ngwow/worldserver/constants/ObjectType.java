package ru.ngwow.worldserver.constants;

public enum ObjectType {
    
    OBJECT(0),
    ITEM(1),
    CONTAINER(2),
    UNIT(3),
    PLAYER(4),
    GAMEOBJECT(5),
    DYNAMICOBJECT(6),
    CORPSE(7);
    
    private int data;
    
    ObjectType(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
