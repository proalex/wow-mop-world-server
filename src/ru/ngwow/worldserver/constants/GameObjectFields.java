package ru.ngwow.worldserver.constants;

public enum GameObjectFields {
    
    CREATED_BY(0X0),
    DISPLAY_ID(0X2),
    FLAGS(0X3),
    PARENT_ROTATION(0X4),
    ANIM_PROGRESS(0X8),
    FACTION_TEMPLATE(0X9),
    LEVEL(0XA),
    PERCENT_HEALTH(0XB),
    END(0XC);
    
    private int data;
    
    GameObjectFields(int data) {
        this.data = ObjectFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
