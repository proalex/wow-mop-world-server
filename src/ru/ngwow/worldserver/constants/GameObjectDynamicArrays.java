package ru.ngwow.worldserver.constants;

public enum GameObjectDynamicArrays {
    
    UNKNOWN_FIELD(0x0),
    END(0x1);
    
    private int data;
    
    GameObjectDynamicArrays(int data) {
        this.data = DynamicObjectArrays.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
