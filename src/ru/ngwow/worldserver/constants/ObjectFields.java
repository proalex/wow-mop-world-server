package ru.ngwow.worldserver.constants;

public enum ObjectFields {
    
    GUID(0x0),
    DATA(0x2),
    TYPE(0x4),
    ENTRY(0x5),
    DYNAMIC_FLAGS(0x6),
    SCALE(0x7),
    END(0x8);
    
    private int data;
    
    ObjectFields(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
