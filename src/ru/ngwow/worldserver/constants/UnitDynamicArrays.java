package ru.ngwow.worldserver.constants;

public enum UnitDynamicArrays {
    
    PASSIVE_SPELLS(0x0),
    WORLD_EFFECTS(0x101),
    END(0x202);
    
    private int data;
    
    UnitDynamicArrays(int data) {
        this.data = DynamicObjectArrays.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
