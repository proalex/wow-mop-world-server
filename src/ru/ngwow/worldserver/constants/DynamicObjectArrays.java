package ru.ngwow.worldserver.constants;

public enum DynamicObjectArrays {
    
    END(0x0);
    
    private int data;
    
    DynamicObjectArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
