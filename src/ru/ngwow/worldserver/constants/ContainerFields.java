package ru.ngwow.worldserver.constants;

public enum ContainerFields {
    
    NAME_SLOTS(0x0),
    SLOTS(0x1),
    END(0x49);
    
    private int data;
    
    ContainerFields(int data) {
        this.data = ItemFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
