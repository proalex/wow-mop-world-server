package ru.ngwow.worldserver.constants;

public enum CharacterFlag {
    
    NONE(0),
    DECLINE(0x2000000);
    
    private int data;
    
    CharacterFlag(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}