package ru.ngwow.worldserver.constants;

public enum AreaTriggerDynamicArrays {
    
    END(DynamicObjectArrays.END.getData());
    
    private int data;
    
    AreaTriggerDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
