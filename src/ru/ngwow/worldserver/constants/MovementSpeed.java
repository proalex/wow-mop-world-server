package ru.ngwow.worldserver.constants;

public class MovementSpeed {
    
    public static final float walkSpeed     = 2.5F;
    public static float runSpeed            = 7F;
    public static float swimSpeed           = 4.72222F;
    public static float swimBackSpeed       = 4.5F;
    public static float runBackSpeed        = 2.5F;
    public static float turnSpeed           = (float) Math.PI;
    public static float flySpeed            = 7F;
    public static float flyBackSpeed        = 4.5F;
    public static float pitchSpeed          = (float) Math.PI;
}
