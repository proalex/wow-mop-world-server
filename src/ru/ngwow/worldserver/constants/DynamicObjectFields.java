package ru.ngwow.worldserver.constants;

public enum DynamicObjectFields {
    
    CASTER(0x0),
    TYPE_AND_VISUAL_ID(0x2),
    SPELL_ID(0x3),
    RADIUS(0x4),
    CAST_TIME(0x5),
    END(0x6);
    
    private int data;
    
    DynamicObjectFields(int data) {
        this.data = ObjectFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
