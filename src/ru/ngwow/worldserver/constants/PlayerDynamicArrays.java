package ru.ngwow.worldserver.constants;

public enum PlayerDynamicArrays {
    
    RESEARCH_SITES(0x0),
    DAILY_QUESTS_COMPLETED(0x2),
    END(0x4);
    
    private int data;
    
    PlayerDynamicArrays(int data) {
        this.data = UnitDynamicArrays.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
