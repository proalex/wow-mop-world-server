package ru.ngwow.worldserver.constants;

public enum UpdateType {
    
    VALUES(0),
    CREATE_OBJECT(1),
    CREATE_OBJECT2(2),
    OUT_OF_RANGE(3);
    
    private int data;
    
    UpdateType(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
