package ru.ngwow.worldserver.constants;

public enum AreaTriggerFields {
    
    CASTER(0x0),
    SPELL_ID(0x2),
    SPELL_VISUAL_ID(0x3),
    DURATION(0x4),
    END(0x5);
    
    private int data;
    
    AreaTriggerFields(int data) {
        this.data = ObjectFields.END.getData() + data;
    }
    
    public int getData() {
        return data;
    }
}
