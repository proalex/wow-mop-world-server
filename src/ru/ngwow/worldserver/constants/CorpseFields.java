package ru.ngwow.worldserver.constants;

public enum CorpseFields {
    
    OWNER(0X0),
    PARTY_GUID(0X2),
    DISPLAY_ID(0X4),
    ITEMS(0X5),
    SKINID(0X18),
    FACIAL_HAIR_STYLE_ID(0X19),
    FLAGS(0X1A),
    DYNAMIC_FLAGS(0X1B),
    END(0X1C);
    
    private int data;
    
    CorpseFields(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
