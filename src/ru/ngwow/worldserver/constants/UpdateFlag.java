package ru.ngwow.worldserver.constants;

public enum UpdateFlag {
    
    SELF(0x0001),
    ALIVE(0x0002),
    ROTATION(0x0004),
    STATIONARY_POSITION(0x0008),
    TARGET(0x0010),
    TRANSPORT(0x0020),
    GO_TRANSPORT_POSITION(0x0040),
    ANIMKITS(0x0080),
    VEHICLE(0x0100);
    
    private int data;
    
    UpdateFlag(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
