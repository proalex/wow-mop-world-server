package ru.ngwow.worldserver.constants;

public enum SceneObjectDynamicArrays {
    
    END(DynamicObjectArrays.END.getData());
    
    private int data;
    
    SceneObjectDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
