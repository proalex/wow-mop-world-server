package ru.ngwow.worldserver.constants;

public enum SessionState {
    
    IN_GAME,
    NOT_IN_GAME,
    ANY;
}
