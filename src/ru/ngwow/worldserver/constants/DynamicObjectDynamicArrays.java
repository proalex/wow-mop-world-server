package ru.ngwow.worldserver.constants;

public enum DynamicObjectDynamicArrays {
    
    END(DynamicObjectArrays.END.getData());
    
    private int data;
    
    DynamicObjectDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
