package ru.ngwow.worldserver.constants;

public enum AccountDataTypes {
    
    GLOBAL_CONFIG_CACHE(0x0),
    CHARACTER_CONFIG_CACHE(0x1),
    GLOBAL_BINDINGS_CACHE(0x2),
    CHARACTER_BINDINGS_CACHE(0x3),
    GLOBAL_MACROS_CACHE(0x4),
    CHARACTER_MACROS_CACHE(0x5),
    CHARACTER_LAYOUT_CACHE(0x6),
    CHARACTER_CHAT_CACHE(0x7);
    
    private int data;
    
    AccountDataTypes(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
