package ru.ngwow.worldserver.constants;

public enum CorpseDynamicArrays {
    
    END(DynamicObjectArrays.END.getData());
    
    private int data;
    
    CorpseDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
