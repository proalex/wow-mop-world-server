package ru.ngwow.worldserver.constants;

public enum ItemDynamicArrays {
    
    END(DynamicObjectArrays.END.getData());
    
    private int data;
    
    ItemDynamicArrays(int data) {
        this.data = data;
    }
    
    public int getData() {
        return data;
    }
}
