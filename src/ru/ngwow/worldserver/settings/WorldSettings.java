package ru.ngwow.worldserver.settings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import ru.ngwow.worldserver.config.Config;
import ru.ngwow.worldserver.exceptions.ConfigFileException;

public class WorldSettings {

    private static Map<String, String> settings = new HashMap<>();
    private static Config config;
    
    private WorldSettings() {
    }

    public static void load() throws IOException, ConfigFileException {
        if (config == null) {
            config = new Config("worldserver.properties");
        }
        settings.clear();
        settings.put("network.ip", config.getProperty("network.ip"));
        settings.put("network.port", config.getProperty("network.port"));
        settings.put("mysql.auth.host", config.getProperty("mysql.auth.host"));
        settings.put("mysql.auth.port", config.getProperty("mysql.auth.port"));
        settings.put("mysql.auth.user", config.getProperty("mysql.auth.user"));
        settings.put("mysql.auth.password", config.getProperty("mysql.auth.password"));
        settings.put("mysql.auth.schema", config.getProperty("mysql.auth.schema"));
        settings.put("mysql.auth.threads", config.getProperty("mysql.auth.threads"));
        settings.put("mysql.characters.host", config.getProperty("mysql.characters.host"));
        settings.put("mysql.characters.port", config.getProperty("mysql.characters.port"));
        settings.put("mysql.characters.user", config.getProperty("mysql.characters.user"));
        settings.put("mysql.characters.password", config.getProperty("mysql.characters.password"));
        settings.put("mysql.characters.schema", config.getProperty("mysql.characters.schema"));
        settings.put("mysql.characters.threads", config.getProperty("mysql.characters.threads"));
        settings.put("mysql.world.host", config.getProperty("mysql.world.host"));
        settings.put("mysql.world.port", config.getProperty("mysql.world.port"));
        settings.put("mysql.world.user", config.getProperty("mysql.world.user"));
        settings.put("mysql.world.password", config.getProperty("mysql.world.password"));
        settings.put("mysql.world.schema", config.getProperty("mysql.world.schema"));
        settings.put("mysql.world.threads", config.getProperty("mysql.world.threads"));
        settings.put("world.id", config.getProperty("world.id"));
        settings.put("thread.acceptor", config.getProperty("thread.acceptor"));
        settings.put("thread.io", config.getProperty("thread.io"));
        settings.put("thread.packethandler", config.getProperty("thread.packethandler"));
        settings.put("motd", config.getProperty("motd"));
        settings.put("path.dbc", config.getProperty("path.dbc"));
        settings.put("debug.packets", config.getProperty("debug.packets"));
        settings.put("debug.unknownopcodes", config.getProperty("debug.unknownopcodes"));
        settings.put("timer.savecharacters", config.getProperty("timer.savecharacters"));
        settings.put("timer.updatevisibilityranges", config.getProperty("timer.updatevisibilityranges"));
    }

    public static String get(String key) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        return settings.get(key);
    }

    public static int getInt(String key) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        return new Integer(settings.get(key));
    }
}
