package ru.ngwow.worldserver.encryption; 

public class SARC4 {

    private byte state[] = new byte[256];
    private byte previousState[] = new byte[256];
    private int previousX;
    private int previousY;
    private int x;
    private int y;

    public synchronized boolean init(byte[] key) {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        for (int i = 0; i < 256; i++) {
            state[i] = (byte) i;
        }
        x = 0;
        y = 0;
        int index1 = 0;
        int index2 = 0;
        byte tmp;
        if (key == null || key.length == 0) {
            throw new NullPointerException();
        }
        for (int i = 0; i < 256; i++) {
            index2 = ((key[index1] & 0xff) + (state[i] & 0xff) + index2) & 0xff;
            tmp = state[i];
            state[i] = state[index2];
            state[index2] = tmp;
            index1 = (index1 + 1) % key.length;
        }
        System.arraycopy(state, 0, previousState, 0, state.length);
        previousX = x;
        previousY = y;
        return true;
    }
    
    public synchronized void setPreviousState() {
        System.arraycopy(previousState, 0, state, 0, state.length);
        x = previousX;
        y = previousY;
    }

    public synchronized byte[] update(byte[] buf) {
        if (buf == null) {
            throw new NullPointerException("buf is null");
        }
        int xorIndex;
        byte tmp;
        if (buf == null) {
            return null;
        }
        System.arraycopy(state, 0, previousState, 0, state.length);
        previousX = x;
        previousY = y;
        byte[] result = new byte[buf.length];
        for (int i = 0; i < buf.length; i++) {
            x = (x + 1) & 0xff;
            y = ((state[x] & 0xff) + y) & 0xff;
            tmp = state[x];
            state[x] = state[y];
            state[y] = tmp;
            xorIndex = ((state[x] & 0xff) + (state[y] & 0xff)) & 0xff;
            result[i] = (byte) (buf[i] ^ state[xorIndex]);
        }
        return result;
    }
}