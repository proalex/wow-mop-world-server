package ru.ngwow.worldserver.encryption;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class Crypt {

    private byte[] EncryptionKey = {
        0x08, (byte) 0xF1, (byte) 0x95, (byte) 0x9F, 0x47, (byte) 0xE5, (byte) 0xD2,
        (byte) 0xDB, (byte) 0xA1, 0x3D, 0x77, (byte) 0x8F, 0x3F, 0x3E, (byte) 0xE7, 0x00};
    private byte[] DecryptionKey = {
        0x40, (byte) 0xAA, (byte) 0xD3, (byte) 0x92, 0x26, 0x71, 0x43, 0x47, (byte) 0x3A,
        0x31, 0x08, (byte) 0xA6, (byte) 0xE7, (byte) 0xDC, (byte) 0x98, (byte) 0x2A};
    private boolean isEnabled = false;
    private final SARC4 DecryptSARC4 = new SARC4();
    private final SARC4 EncryptSARC4 = new SARC4();

    public Crypt() {
    }

    public byte[] encrypt(final byte[] data) {
        if (data == null) {
            throw new NullPointerException("data is null");
        }
        if (!isEnabled) {
            return data;
        }
        return EncryptSARC4.update(data);
    }
    
    public byte[] decrypt(final byte[] data) {
        if (data == null) {
            throw new NullPointerException("data is null");
        }
        if (!isEnabled) {
            return data;
        }
        return DecryptSARC4.update(data);
    }
    
    public void setPreviousState() {
        DecryptSARC4.setPreviousState();
    }

    public void init(final byte[] key)
            throws NoSuchAlgorithmException, InvalidKeyException {
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        final byte[] encryptHash = getKey(EncryptionKey, key);
        final byte[] decryptHash = getKey(DecryptionKey, key);
        final byte[] EncryptDummy = new byte[1024];
        final byte[] DecryptDummy = new byte[1024];
        EncryptSARC4.init(encryptHash);
        DecryptSARC4.init(decryptHash);
        EncryptSARC4.update(EncryptDummy);
        DecryptSARC4.update(DecryptDummy);
        isEnabled = true;
    }

    private byte[] getKey(final byte[] EncryptionKey, final byte[] key)
            throws NoSuchAlgorithmException, InvalidKeyException {
        if (EncryptionKey == null) {
            throw new NullPointerException("EncryptionKey is null");
        }
        if (key == null) {
            throw new NullPointerException("key is null");
        }
        final SecretKeySpec ds = new SecretKeySpec(EncryptionKey, "HmacSHA1");
        Mac m;
        m = Mac.getInstance("HmacSHA1");
        m.init(ds);
        return m.doFinal(key);
    }
    
    public boolean isEnabled() {
        return isEnabled;
    }
}
