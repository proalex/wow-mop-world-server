package ru.ngwow.worldserver.entities;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ru.ngwow.worldserver.constants.HighGuidType;
import ru.ngwow.worldserver.constants.ObjectFields;
import ru.ngwow.worldserver.constants.UnitFields;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.objectdefines.SmartGuid;
import ru.ngwow.worldserver.objectdefines.Vector4;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;

public class Creature extends WorldObject {

    private int id;
    public String name;
    public String subName;
    public String iconName;
    public int[] flag = new int[2];
    public int type;
    public int family;
    public int rank;
    public int[] questKillNpcId = new int[2];
    public int[] displayInfoId = new int[4];
    public float healthModifier;
    public float powerModifier;
    public byte racialLeader;
    public int[] questItemId = new int[6];
    public int movementInfoId;
    public int expansionRequired;
    private int health;
    private byte level;
    private byte classid;
    private int faction;
    private float scale;
    private int unitFlags;
    private int unitFlags2;
    private int npcFlags;
    private SmartGuid smartGuid;

    public Creature(int id, long guid, int map, Vector4 position) throws Exception {
        super(UnitFields.END.getData());
        this.id = id;
        this.guid = guid;
        this.map = map;
        setPosition(position);
        guid = new SmartGuid(uint64(guid), id, HighGuidType.UNIT).getGuid().longValue();
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getWorldPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCreatureStats);
            statment.setInt(1, id);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getWorldPool().returnObject(current);
            }
        }
        if (!result.next()) {
            throw new IOException("creature stats for id " + id + " not found");
        }
        name = result.getString("name");
        subName = result.getString("subName");
        iconName = result.getString("iconName");
        for (int i = 0; i < flag.length; i++) {
            flag[i] = result.getInt("flag" + (i + 1 != 1 ? i + 1 : ""));
        }
        type = result.getInt("type");
        family  = result.getInt("family");
        rank = result.getInt("rank");
        for (int i = 0; i < questKillNpcId.length; i++) {
            questKillNpcId[i] = result.getInt("questKillNpcId" + (i + 1 != 1 ? i + 1 : ""));
        }
        for (int i = 0; i < displayInfoId.length; i++) {
            displayInfoId[i] = result.getInt("displayInfoId" + (i + 1 != 1 ? i + 1 : ""));
        }
        healthModifier = result.getFloat("healthModifier");
        powerModifier = result.getFloat("powerModifier");
        racialLeader = result.getByte("racialLeader");
        for (int i = 0; i < questItemId.length; i++) {
            questItemId[i] = result.getInt("questItemId" + (i + 1 != 1 ? i + 1 : ""));
        }
        movementInfoId = result.getInt("movementInfoId");
        expansionRequired = result.getInt("expansionRequired");
        try {
            current = dbConnection.getWorldPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCreatureData);
            statment.setInt(1, id);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getWorldPool().returnObject(current);
            }
        }
        if (!result.next()) {
            throw new IOException("creature data not found");
        }
        health = result.getInt("health");
        level = result.getByte("level");
        classid = result.getByte("class");
        faction = result.getInt("faction");
        scale = result.getInt("scale");
        unitFlags = result.getInt("unitFlags");
        unitFlags2 = result.getInt("unitFlags2");
        npcFlags = result.getInt("npcFlags");
    }

    @Override
    public void setUpdateFields() throws IOException {
        setUpdateField(ObjectFields.GUID.getData(), uint64(guid));
        setUpdateField(ObjectFields.DATA.getData(), uint64(0));
        setUpdateField(ObjectFields.ENTRY.getData(), id);
        setUpdateField(ObjectFields.TYPE.getData(), 0x9);
        setUpdateField(ObjectFields.SCALE.getData(), scale);
        setUpdateField(UnitFields.CHARM.getData(), uint64(1));
        setUpdateField(UnitFields.SUMMON.getData(), uint64(1));
        setUpdateField(UnitFields.CRITTER.getData(), uint64(0));
        setUpdateField(UnitFields.CHARMED_BY.getData(), uint64(0));
        setUpdateField(UnitFields.SUMMONED_BY.getData(), uint64(0));
        setUpdateField(UnitFields.CREATED_BY.getData(), uint64(0));
        setUpdateField(UnitFields.TARGET.getData(), uint64(0));
        setUpdateField(UnitFields.CHANNEL_OBJECT.getData(), uint64(0));
        setUpdateField(UnitFields.HEALTH.getData(), health);
        for (int i = 0; i < 5; i++) {
            setUpdateField(UnitFields.POWER.getData() + i, 0);
        }
        setUpdateField(UnitFields.MAX_HEALTH.getData(), health);
        for (int i = 0; i < 5; i++) {
            setUpdateField(UnitFields.MAX_POWER.getData() + i, 0);
        }
        setUpdateField(UnitFields.POWER_REGEN_FLAT_MODIFIER.getData(), 0);
        setUpdateField(UnitFields.POWER_REGEN_INTERRUPTED_FLAT_MODIFIER.getData(), 0);
        setUpdateField(UnitFields.BASE_HEALTH.getData(), 1);
        setUpdateField(UnitFields.BASE_MANA.getData(), 0);
        setUpdateField(UnitFields.LEVEL.getData(), level);
        setUpdateField(UnitFields.FACTION_TEMPLATE.getData(), faction);
        setUpdateField(UnitFields.FLAGS.getData(), unitFlags);
        setUpdateField(UnitFields.FLAGS2.getData(), unitFlags2);
        setUpdateField(UnitFields.NPC_FLAGS.getData(), npcFlags);
        for (int i = 0; i < 5; i++) {
            setUpdateField(UnitFields.STATS.getData() + i, 0);
            setUpdateField(UnitFields.STAT_POS_BUFF.getData() + i, 0);
            setUpdateField(UnitFields.STAT_NEG_BUFF.getData() + i, 0);
        }
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), (byte) 0, 0);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), (byte) 0, 1);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), (byte) 0, 2);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), (byte) 0, 3);
        setUpdateField(UnitFields.DISPLAY_ID.getData(), displayInfoId[0]);
        setUpdateField(UnitFields.NATIVE_DISPLAY_ID.getData(), displayInfoId[2]);
        setUpdateField(UnitFields.MOUNT_DISPLAY_ID.getData(), 0);
        setUpdateField(UnitFields.BOUND_IN_RADIUS.getData(), (float) 0.389F);
        setUpdateField(UnitFields.COMBAT_REACH.getData(), (float) 1.5F);
        setUpdateField(UnitFields.MIN_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.MAX_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.MOD_CASTING_SPEED.getData(), (float) 1);
        setUpdateField(UnitFields.ATTACK_POWER.getData(), 0);
        setUpdateField(UnitFields.RANGED_ATTACK_POWER.getData(), 0);
        for (int i = 0; i < 7; i++) {
            setUpdateField(UnitFields.RESISTANCES.getData() + i, 0);
            setUpdateField(UnitFields.RESISTANCE_BUFF_MODS_POSITIVE.getData() + i, 0);
            setUpdateField(UnitFields.RESISTANCE_BUFF_MODS_NEGATIVE.getData() + i, 0);
        }
        setUpdateField(UnitFields.ANIM_TIER.getData(), (byte) 0, 0);
        setUpdateField(UnitFields.ANIM_TIER.getData(), (byte) 0, 1);
        setUpdateField(UnitFields.ANIM_TIER.getData(), (byte) 0, 2);
        setUpdateField(UnitFields.ANIM_TIER.getData(), (byte) 0, 3);
        setUpdateField(UnitFields.RANGED_ATTACK_ROUND_BASE_TIME.getData(), 0);
        setUpdateField(UnitFields.RANGED_ATTACK_ROUND_BASE_TIME.getData(), 0, 1);
        setUpdateField(UnitFields.MIN_OFF_HAND_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.MAX_OFF_HAND_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.ATTACK_POWER_MOD_POS.getData(), 0);
        setUpdateField(UnitFields.RANGED_ATTACK_POWER_MOD_POS.getData(), 0);
        setUpdateField(UnitFields.MIN_RANGED_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.MAX_RANGED_DAMAGE.getData(), (float) 0);
        setUpdateField(UnitFields.ATTACK_POWER_MULTIPLIER.getData(), (float) 0);
        setUpdateField(UnitFields.RANGED_ATTACK_POWER_MULTIPLIER.getData(), (float) 0);
        setUpdateField(UnitFields.MAX_HEALTH_MODIFIER.getData(), (float) 1);
    }
}
