package ru.ngwow.worldserver.entities;

import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;
import ru.ngwow.worldserver.objectdefines.Vector4;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.utils.uint.UInt32;
import ru.ngwow.worldserver.utils.uint.UInt64;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;
import ru.ngwow.worldserver.utils.variabletype.VariableType;

public abstract class WorldObject {
    
    protected long guid;
    protected int map;
    Vector4 position = new Vector4();
    protected long targetGuid;
    private int maskSize;
    private BitSet mask;
    private HashMap updateData = new HashMap();
    protected boolean isInGame = true;
    protected boolean isPosChanged = false;
    
    public WorldObject(int dataLength) {
        maskSize = (dataLength + 32) / 32;
        mask = new BitSet(dataLength);
    }
    
    public boolean checkDistance(WorldObject object, float dist) {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (getMap() == object.getMap()) {
            return position.getDistance(object.getPosition()) <= dist;
        }
        return false;
    }
    
    public boolean checkDistance(WorldObject object) {
        return checkDistance(object, 10000);
    }
    
    public abstract void setUpdateFields() throws IOException;
    
    public synchronized void setUpdateField(int index, Object value, int offset) {
        if (value == null) {
            throw new NullPointerException("value is null");
        }
        VariableType type = VariableType.getType(value.getClass().getCanonicalName());
        switch(type) {
            case UINT32:
            case FLOAT:
            case INT32:
                mask.set(index, true);
                updateData.put(index, value);
                break;
            case BYTE:
                mask.set(index, true);
                if (updateData.containsKey(index)) {
                    int objValue = (Byte) value;
                    type = VariableType.getType(updateData.get(index).getClass().getCanonicalName());
                    switch (type) {
                        case UINT32:
                            objValue = ((UInt32)updateData.get(index)).intValue() | objValue << (offset * 8);
                            updateData.put(index, uint32(objValue));
                            break;
                        case INT32:
                            objValue = ((Integer)updateData.get(index)) | objValue << (offset * 8);
                            updateData.put(index, objValue);
                            break;
                    }
                } else {
                    int objValue = (Byte) value;
                    objValue = objValue << (offset * 8);
                    updateData.put(index, objValue);
                }
                break;
            case UINT16:
                mask.set(index, true);
                if (updateData.containsKey(index)) {
                    int objValue = (Short) value;
                    type = VariableType.getType(updateData.get(index).getClass().getCanonicalName());
                    switch (type) {
                        case UINT32:
                            objValue = ((UInt32)updateData.get(index)).intValue() | objValue << (offset * 16);
                            updateData.put(index, uint32(objValue));
                            break;
                        case INT32:
                            objValue = ((Integer)updateData.get(index)) | objValue << (offset * 16);
                            updateData.put(index, objValue);
                            break;
                    }
                } else {
                    int objValue = (Byte) value;
                    objValue = objValue << (offset * 8);
                    updateData.put(index, objValue);
                }
                break;
            case UINT64:
                mask.set(index, true);
                mask.set(index + 1, true);
                long objValue = ((UInt64) value).longValue();
                updateData.put(index, uint32(objValue & UInt32.MAX_VALUE));
                updateData.put(index + 1, uint32((objValue >>> 32) & UInt32.MAX_VALUE));
                break;
        }
    }
    
    public void setUpdateField(int index, Object value) {
        setUpdateField(index, value, 0);
    }
    
    public synchronized void writeUpdateFields(Packet packet) throws IOException {
        if (packet == null) {
            throw new NullPointerException("packet is null");
        }
        packet.writeUInt8(maskSize);
        byte[] maskByteArray = mask.toByteArray();
        byte[] maskData = new byte[mask.size() / 8];
        System.arraycopy(maskByteArray, 0, maskData, 0, maskByteArray.length);
        packet.writeBytes(maskData);
        for (int i = 0; i < mask.size(); i++) {
            if (mask.get(i)) {
                VariableType type = VariableType.getType(updateData.get(i).getClass().getCanonicalName());
                switch(type) {
                    case UINT32:
                        packet.writeUInt32((UInt32)updateData.get(i));
                        break;
                    case FLOAT:
                        packet.writeFloat((Float)updateData.get(i));
                        break;
                    case INT32:
                        packet.writeInt32((Integer)updateData.get(i));
                        break;
                }
            }
        }
        mask.set(0, mask.length() - 1, false);
    }
    
    public void setInGame(boolean state) {
        isInGame = state;
    }
    
    public synchronized void writeDynamicUppdateFields(Packet packet) {
        if (packet == null) {
            throw new NullPointerException("packet is null");
        }
        packet.writeUInt8(0);
    }
    
    public void removeFromWorld() {
    }
    
    public Player toPlayer() {
        return (Player) this;
    }
    
    public boolean isInGame() {
        return isInGame;
    }

    public long getGuid() {
        return guid;
    }

    public int getMap() {
        return map;
    }

    public long getTargetGuid() {
        return targetGuid;
    }
    
    public Vector4 getPosition() {
        return position;
    }
    
    public void setPosition(Vector4 position) {
        if (position == null) {
            throw new NullPointerException("position is null");
        }
        this.position = position;
        isPosChanged = true;
    }
    
    public boolean equals(Object object) {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (!(object instanceof WorldObject)) {
            return false;
        }
        WorldObject worldObject = (WorldObject) object;
        return guid == worldObject.guid ? true : false;
    }
}
