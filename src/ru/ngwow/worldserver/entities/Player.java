package ru.ngwow.worldserver.entities;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import ru.ngwow.worldserver.clientdb.query.DBCQuery;
import ru.ngwow.worldserver.constants.Faction;
import ru.ngwow.worldserver.constants.ObjectFields;
import ru.ngwow.worldserver.constants.PlayerFields;
import ru.ngwow.worldserver.constants.UnitFields;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.managers.ActionMgr;
import ru.ngwow.worldserver.managers.SkillMgr;
import ru.ngwow.worldserver.managers.SpecializationMgr;
import ru.ngwow.worldserver.managers.SpellMgr;
import ru.ngwow.worldserver.objectdefines.ActionButton;
import ru.ngwow.worldserver.objectdefines.Talent;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.uint.UInt32;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;

public class Player extends WorldObject {
    
    private int accountId;
    private String name;
    private byte race;
    private byte classid;
    private byte gender;
    private byte skin;
    private byte face;
    private byte hairStyle;
    private byte hairColor;
    private byte facialHair;
    private byte level;
    private int zone;
    private long guildGuid;
    private int petDisplayInfo;
    private int petLevel;
    private int petFamily;
    private int characterFlags;
    private int customizeFlags;
    private boolean loginCinematic;
    private int specGroupCount;
    private int activeSpecGroup;
    private int primarySpec;
    private int secondarySpec;
    private ArrayList<Talent> talents = new ArrayList<>();
    private ArrayList<Skill> skills = new ArrayList<>();
    private ArrayList<ActionButton> actionButtons = new ArrayList<>();
    private ArrayList<Spell> spells = new ArrayList<>();
    private WorldSession session;
    private Vector<WorldObject> inRangeObjects = new Vector<>();
    private Faction faction;
    
    public Player(WorldSession session, long guid) throws Exception {
        super(PlayerFields.END.getData());
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        this.guid = guid;
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacterData);
            statment.setLong(1, guid);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (!result.next()) {
            throw new IOException("character not found");
        }
        guid            = result.getLong("Guid");
        accountId       = result.getInt("AccountId");
        name            = result.getString("Name");
        race            = result.getByte("Race");
        classid         = result.getByte("Class");
        gender          = result.getByte("Gender");
        skin            = result.getByte("Skin");
        face            = result.getByte("Face");
        hairStyle       = result.getByte("HairStyle");
        hairColor       = result.getByte("HairColor");
        facialHair      = result.getByte("FacialHair");
        level           = result.getByte("Level");
        zone            = result.getInt("Zone");
        map             = result.getInt("Map");
        position.x      = result.getFloat("X");
        position.y      = result.getFloat("Y");
        position.z      = result.getFloat("Z");
        position.o      = result.getFloat("O");
        guildGuid       = result.getLong("GuildGuid");
        petDisplayInfo  = result.getInt("PetDisplayId");
        petLevel        = result.getInt("PetLevel");
        petFamily       = result.getInt("PetFamily");
        characterFlags  = result.getInt("CharacterFlags");
        customizeFlags  = result.getInt("CustomizeFlags");
        loginCinematic  = result.getBoolean("LoginCinematic");
        specGroupCount  = result.getInt("SpecGroupCount");
        activeSpecGroup = result.getInt("ActiveSpecGroup");
        primarySpec     = result.getInt("PrimarySpecId");
        secondarySpec   = result.getInt("SecondarySpecId");
        this.session = session;
        if (race == 22 || race == 7 || race == 3 || race == 11 || race == 4 || race == 25 || race == 1) {
            faction = Faction.ALIANCE;
        } else if (race == 9 || race == 5 || race == 2 || race == 26 || race == 6 || race == 8 || race == 10) {
            faction = Faction.HORDE;
        } else {
            faction = Faction.NEUTRAL;
        }
        SpecializationMgr.loadTalents(this);
        SkillMgr.loadSkills(this);
        ActionMgr.loadActionButtons(this);
        SpellMgr.loadSpells(this);
    }
    
    @Override
    public synchronized void setUpdateFields() {
        setUpdateField(ObjectFields.GUID.getData(), uint64(getGuid()));
        setUpdateField(ObjectFields.DATA.getData(), uint64(0));
        setUpdateField(ObjectFields.TYPE.getData(), 0x19);
        setUpdateField(ObjectFields.DYNAMIC_FLAGS.getData(), 0);
        setUpdateField(ObjectFields.SCALE.getData(), 1.0F);
        setUpdateField(UnitFields.HEALTH.getData(), 123);
        setUpdateField(UnitFields.MAX_HEALTH.getData(), 123);
        setUpdateField(UnitFields.LEVEL.getData(), level);
        UInt32 faction = DBCQuery.getFaction(race);
        if (faction == null) {
            throw new NullPointerException("faction is null");
        }
        setUpdateField(UnitFields.FACTION_TEMPLATE.getData(), faction);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), race, 0);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), classid, 1);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), gender, 2);
        setUpdateField(UnitFields.DISPLAY_POWER.getData(), (byte) 0, 3);
        UInt32 displayId = DBCQuery.getDisplayId(race, gender);
        if (displayId == null) {
            throw new NullPointerException("displayId is null");
        }
        setUpdateField(UnitFields.DISPLAY_ID.getData(), displayId);
        setUpdateField(UnitFields.NATIVE_DISPLAY_ID.getData(), displayId);
        setUpdateField(UnitFields.FLAGS.getData(), uint32(0x8));
        setUpdateField(UnitFields.BOUND_IN_RADIUS.getData(), 0.389F);
        setUpdateField(UnitFields.COMBAT_REACH.getData(), 1.5F);
        setUpdateField(UnitFields.MOD_CASTING_SPEED.getData(), 1F);
        setUpdateField(UnitFields.MAX_HEALTH_MODIFIER.getData(), 1F);
        setUpdateField(PlayerFields.HAIR_COLOR_ID.getData(), skin, 0);
        setUpdateField(PlayerFields.HAIR_COLOR_ID.getData(), face, 1);
        setUpdateField(PlayerFields.HAIR_COLOR_ID.getData(), hairStyle, 2);
        setUpdateField(PlayerFields.HAIR_COLOR_ID.getData(), hairColor, 3);
        setUpdateField(PlayerFields.REST_STATE.getData(), facialHair, 0);
        setUpdateField(PlayerFields.REST_STATE.getData(), (byte) 0, 1);
        setUpdateField(PlayerFields.REST_STATE.getData(), (byte) 0, 2);
        setUpdateField(PlayerFields.REST_STATE.getData(), (byte) 2, 3);
        setUpdateField(PlayerFields.ARENA_FACTION.getData(), gender, 0);
        setUpdateField(PlayerFields.ARENA_FACTION.getData(), (byte) 0, 1);
        setUpdateField(PlayerFields.ARENA_FACTION.getData(), (byte) 0, 2);
        setUpdateField(PlayerFields.ARENA_FACTION.getData(), (byte) 0, 3);
        setUpdateField(PlayerFields.WATCHED_FACTION_INDEX.getData(), -1);
        setUpdateField(PlayerFields.XP.getData(), 0);
        setUpdateField(PlayerFields.NEXT_LEVEL_XP.getData(), 400);
        setUpdateField(PlayerFields.CURRENT_SPEC_ID.getData(), getActiveSpecId());
        setUpdateField(PlayerFields.SPELL_CRIT_PERCENTAGE.getData() + 0,
                SpecializationMgr.getUnspentTalentRowCount(this), 0);
        setUpdateField(PlayerFields.SPELL_CRIT_PERCENTAGE.getData() + 1,
                SpecializationMgr.getMaxTalentRowCount(this), 0);
        for (int i = 0; i < 448; i++) {
            if (i < skills.size()) {
                setUpdateField(PlayerFields.SKILL.getData() + i, uint32(skills.get(i).getId()));
            }
        }
        setUpdateField(PlayerFields.VIRTUAL_PLAYER_REALM.getData(), uint32(WorldSettings.getInt("world.id")));
    }
    
    public void setZone(int zone) throws Exception {
        this.zone = zone;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.setPlayerZone);
            statment.setInt(1, zone);
            statment.setLong(2, guid);
            statment.execute();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
    }
    
    public void savePositionToDB() throws Exception {
        if (isPosChanged) {
            isPosChanged = false;
            Object current = null;
            MySqlConnection dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.setPlayerPosition);
                statment.setFloat(1, position.x);
                statment.setFloat(2, position.y);
                statment.setFloat(3, position.z);
                statment.setFloat(4, position.o);
                statment.setLong(5, guid);
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
        }
    }
    
    public int getActiveSpecId() {
        if ((activeSpecGroup == 0 && primarySpec == 0) || (activeSpecGroup == 1 && secondarySpec == 0)) {
            return 0;
        }
        return (activeSpecGroup == 0 && primarySpec != 0) ? primarySpec : secondarySpec;
    }

    public int getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }

    public byte getRace() {
        return race;
    }

    public byte getClassid() {
        return classid;
    }

    public byte getGender() {
        return gender;
    }

    public byte getSkin() {
        return skin;
    }

    public byte getFace() {
        return face;
    }

    public byte getHairStyle() {
        return hairStyle;
    }

    public byte getHairColor() {
        return hairColor;
    }

    public byte getFacialHair() {
        return facialHair;
    }

    public byte getLevel() {
        return level;
    }

    public int getZone() {
        return zone;
    }

    public long getGuildGuid() {
        return guildGuid;
    }

    public int getPetDisplayInfo() {
        return petDisplayInfo;
    }

    public int getPetLevel() {
        return petLevel;
    }

    public int getPetFamily() {
        return petFamily;
    }

    public int getCharacterFlags() {
        return characterFlags;
    }

    public int getCustomizeFlags() {
        return customizeFlags;
    }

    public boolean isLoginCinematic() {
        return loginCinematic;
    }

    public int getSpecGroupCount() {
        return specGroupCount;
    }

    public int getActiveSpecGroup() {
        return activeSpecGroup;
    }

    public int getPrimarySpec() {
        return primarySpec;
    }

    public int getSecondarySpec() {
        return secondarySpec;
    }

    public ArrayList<Talent> getTalents() {
        return talents;
    }
    
    public ArrayList<Skill> getSkills() {
        return skills;
    }

    public WorldSession getSession() {
        return session;
    }

    public ArrayList<ActionButton> getActionButtons() {
        return actionButtons;
    }
    
    public ArrayList<Spell> getSpells() {
        return spells;
    }
    
    public Vector<WorldObject> getInRangeObjects() {
        return inRangeObjects;
    }
    
    public Faction getFaction() {
        return faction;
    }
}