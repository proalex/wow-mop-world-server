package ru.ngwow.worldserver.entities;

public class Spell {
    
    private int spellId;
    
    public Spell(int spellId) {
        this.spellId = spellId;
    }
    
    public int getSpellId() {
        return spellId;
    }
}
