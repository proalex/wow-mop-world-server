package ru.ngwow.worldserver.entities;

public class Skill {
    
    private int id;
    private int skillLevel;
    
    public Skill(int id, int skillLevel) {
        this.id = id;
        this.skillLevel = skillLevel;
    }

    public int getId() {
        return id;
    }

    public int getSkillLevel() {
        return skillLevel;
    }
}
