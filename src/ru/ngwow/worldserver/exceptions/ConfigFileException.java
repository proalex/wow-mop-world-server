package ru.ngwow.worldserver.exceptions;

public class ConfigFileException extends Exception {

    public ConfigFileException(String msg) {
        super(msg);
    }
}
