package ru.ngwow.worldserver.exceptions;

public class DBCInvalidGameBuildException extends Exception {

    public DBCInvalidGameBuildException(String msg) {
        super(msg);
    }
}
