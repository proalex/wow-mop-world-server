package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.uint.UInt32;
import ru.ngwow.worldserver.utils.array.ArrayUtils;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.bits.BitUnpack;

public class CacheHandler {
    
    public static void queryRealmName(WorldSession session, Packet input)
            throws Exception {
        UInt32 realmId = input.readUInt32();
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getRealmName);
            statment.setInt(1, realmId.intValue());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        if (result.next()) {
            Packet output = new Packet(ServerOpcode.REALM_QUERY_RESPONCE);
            BitPack bout = new BitPack(output);
            String realmName = result.getString("name");
            output.writeUInt32(realmId);
            output.writeUInt8(0);
            bout.write(realmName.length(), 8);
            bout.write(realmName.length(), 8);
            bout.write(1);
            bout.flush();
            output.writeString(realmName);
            output.writeString(realmName);
            Channel channel = session.getChannel();
            channel.write(output);
        }
    }
    
    public static void queryPlayerName(WorldSession session, Packet input) 
            throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        boolean[] guidMask = new boolean[8];
        byte[] guidBytes = new byte[8];
        int[] guidIndex = {6, 0, 2, 3, 4, 5, 7, 1};
        boolean hasUnknown = false;
        boolean hasUnknown2 = false;
        BitUnpack bin = new BitUnpack(input);
        guidMask[3] = bin.read();
        guidMask[1] = bin.read();
        guidMask[4] = bin.read();
        guidMask[2] = bin.read();
        guidMask[7] = bin.read();
        guidMask[0] = bin.read();
        guidMask[5] = bin.read();
        hasUnknown = bin.read();
        guidMask[6] = bin.read();
        hasUnknown2 = bin.read();
        for (int i = 0; i < guidIndex.length; i++) {
            if (guidMask[guidIndex[i]]) {
                guidBytes[guidIndex[i]] = (byte)(input.readByte() ^ 1);
            }
        }
        if (hasUnknown2) {
            input.skipBytes(4);
        }
        if (hasUnknown) {
            input.skipBytes(4);
        }
        long guid = ArrayUtils.byteArrayToLong(guidBytes);
        WorldSession target = WorldMgr.getSession(guid);
        if (target != null) {
            Channel channel = session.getChannel();
            Player targetPlayer = target.getPlayer();
            if (targetPlayer != null) {
                Packet output = new Packet(ServerOpcode.QUERY_PLAYER_NAME_RESPONSE);
                BitPack bout = new BitPack(output);
                bout.setGuid(guid);
                bout.write(0);
                bout.writeGuidMask(1, 3, 2);
                bout.write(targetPlayer.getName().length(), 6);
                bout.writeGuidMask(6, 4, 0);
                bout.write(0);
                bout.writeGuidMask(5, 7);
                bout.flush();
                bout.writeGuidBytes(1);
                output.writeString(targetPlayer.getName());
                bout.writeGuidBytes(0, 7);
                output.writeUInt8(targetPlayer.getRace());
                output.writeUInt8(0);
                output.writeUInt8(targetPlayer.getGender());
                output.writeUInt8(targetPlayer.getClassid());
                bout.writeGuidBytes(4, 6, 5);
                output.writeUInt32(1);
                bout.writeGuidBytes(3, 2);
                channel.write(output);
            }
        }
    }
}
