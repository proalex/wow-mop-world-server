package ru.ngwow.worldserver.handlers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.clientdb.query.DBCQuery;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.uint.UInt32;

public class CinematicHandler {
    
    private CinematicHandler() {
        
    }
    
    public static void startCinematic(WorldSession session) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Object current = null;
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.START_CINEMATIC);
        Player player = session.getPlayer();
        UInt32 cinematicSequence = DBCQuery.getCinematicSequence(player.getRace());
        if (cinematicSequence == null) {
            throw new NullPointerException("cinematicSequence is null");
        }
        output.writeUInt32(cinematicSequence);
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.updateCinematic);
            statment.setLong(1, player.getGuid());
            statment.execute();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        channel.write(output);
    }
}
