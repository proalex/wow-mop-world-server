package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.SessionState;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;

public class LogoutHandler {
    
    private LogoutHandler() {
    }
    
    public static void logoutRequest(WorldSession session, Packet input)
            throws Exception {
        Packet output = new Packet(ServerOpcode.LOGOUT_COMPLETE);
        Channel channel = session.getChannel();
        session.setState(SessionState.NOT_IN_GAME);
        channel.write(output);
        WorldMgr.removeSession(session);
    }
}
