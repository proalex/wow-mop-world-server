package ru.ngwow.worldserver.handlers;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.AccountDataMasks;
import ru.ngwow.worldserver.constants.CharacterCreateResult;
import ru.ngwow.worldserver.constants.CharacterFlag;
import ru.ngwow.worldserver.constants.SessionState;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.data.CharacterData;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.array.ArrayUtils;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.bits.BitUnpack;

public class CharacterHandler {

    private static final Logger log = Logger.getLogger(CharacterHandler.class.getName());

    private CharacterHandler() {
    }

    public static void enumCharactersResult(WorldSession session) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.ENUM_CHARACTERS_RESULT);
        session.setOnline(true);
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacters);
            statment.setInt(1, session.getId());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        ArrayList<CharacterData> characters = new ArrayList<>();
        while (result.next()) {
            characters.add(new CharacterData(
                    result.getLong("guid"),
                    result.getInt("accountid"),
                    result.getString("name"),
                    result.getInt("race"),
                    result.getInt("class"),
                    result.getInt("gender"),
                    result.getInt("skin"),
                    result.getInt("face"),
                    result.getInt("hairStyle"),
                    result.getInt("hairColor"),
                    result.getInt("facialHair"),
                    result.getInt("level"),
                    result.getInt("zone"),
                    result.getInt("map"),
                    result.getFloat("x"),
                    result.getFloat("y"),
                    result.getFloat("z"),
                    result.getFloat("o"),
                    result.getLong("guildGuid"),
                    result.getInt("petDisplayId"),
                    result.getInt("petLevel"),
                    result.getInt("petFamily"),
                    result.getInt("characterFlags"),
                    result.getInt("customizeFlags"),
                    result.getBoolean("loginCinematic"),
                    result.getInt("specGroupCount"),
                    result.getInt("activeSpecGroup"),
                    result.getInt("primarySpecId"),
                    result.getInt("secondarySpecId")));
        }
        BitPack bout = new BitPack(output);
        bout.write(1);
        bout.write(characters.size(), 16);
        if (!characters.isEmpty()) {
            for (int i = 0; i < characters.size(); i++) {
                CharacterData ch = characters.get(i);
                bout.setGuildGuid(ch.getGuildGuid());
                bout.setGuid(ch.getGuid());
                bout.writeGuidMask(1);
                bout.writeGuildGuidMask(5, 7, 6);
                bout.writeGuidMask(5);
                bout.writeGuildGuidMask(3);
                bout.writeGuidMask(2);
                bout.writeGuildGuidMask(4);
                bout.writeGuidMask(7);
                bout.write(ch.getName().getBytes(Charset.forName("UTF-8")).length, 6);
                bout.write(ch.isLoginCinematic());
                bout.writeGuildGuidMask(1);
                bout.writeGuidMask(4);
                bout.writeGuildGuidMask(2, 0);
                bout.writeGuidMask(6, 3, 0);
            }
            bout.write(0, 21);
            bout.flush();
            for (int i = 0; i < characters.size(); i++) {
                CharacterData ch = characters.get(i);
                bout.setGuildGuid(ch.getGuildGuid());
                bout.setGuid(ch.getGuid());
                bout.writeGuidBytes(4);
                output.writeUInt8(ch.getRace());
                bout.writeGuidBytes(6);
                bout.writeGuildGuidBytes(1);   
                output.writeUInt8(0);
                output.writeUInt8(ch.getHairStyle());
                bout.writeGuildGuidBytes(6);
                bout.writeGuidBytes(3);
                output.writeFloat(ch.getX());
                output.writeUInt32(ch.getCharacterFlags());
                bout.writeGuildGuidBytes(0);
                output.writeUInt32(ch.getPetLevel());
                output.writeUInt32(ch.getMap());
                bout.writeGuildGuidBytes(7);
                output.writeUInt32(ch.getCustomizeFlags());
                bout.writeGuildGuidBytes(4);
                bout.writeGuidBytes(2, 5);
                output.writeFloat(ch.getY());
                output.writeUInt32(ch.getPetFamily());
                output.writeString(ch.getName());
                output.writeUInt32(ch.getPetDisplayId());
                bout.writeGuildGuidBytes(3);
                bout.writeGuidBytes(7);
                output.writeUInt8(ch.getLevel());
                bout.writeGuidBytes(1);
                bout.writeGuildGuidBytes(2);
                for (int j = 0; j < 23; j++) {
                    output.writeUInt32(0);
                    output.writeUInt32(0);
                    output.writeUInt8(0);
                }
                output.writeFloat(ch.getZ());
                output.writeUInt32(ch.getZone());
                output.writeUInt8(ch.getFacialHair());
                output.writeUInt8(ch.getClassId());
                bout.writeGuildGuidBytes(5);
                output.writeUInt8(ch.getSkin());
                output.writeUInt8(ch.getGender());
                output.writeUInt8(ch.getFace());
                bout.writeGuidBytes(0);
                output.writeUInt8(ch.getHairColor());
            }
        } else {
            bout.write(0, 21);
            bout.flush();
        }
        channel.write(output);
    }

    public static void createCharacter(WorldSession session, Packet input) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.CREATE_CHARACTER);
        byte hairStyle = input.readInt8();
        byte face = input.readInt8();
        byte facialHair = input.readInt8();
        byte hairColor = input.readInt8();
        byte race = input.readInt8();
        byte classid = input.readInt8();
        byte skin = input.readInt8();
        input.skipBytes(1);
        byte gender = input.readInt8();
        BitUnpack bin = new BitUnpack(input);
        int nameLength = bin.read(7)/2;
        String name = input.readString(nameLength);
        String normalizedName = name.substring(0, 1).toUpperCase() + name.substring(1, name.length()).toLowerCase();
        if (normalizedName.length() < 2 || normalizedName.length() > 12) {
            output.writeUInt8(CharacterCreateResult.FAILED.getData());
            channel.write(output);
            return;
        }
        if (!normalizedName.matches("[A-Za-z]+")) {
            output.writeUInt8(CharacterCreateResult.FAILED.getData());
            channel.write(output);
            return;
        }
        if (gender != 0 && gender != 1) {
            output.writeUInt8(CharacterCreateResult.FAILED.getData());
            channel.write(output);
            return;
        }
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.checkCharName);
            statment.setString(1, normalizedName);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (result.next()) {
            output.writeUInt8(CharacterCreateResult.NAME_IN_USE.getData());
        } else {
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getAuthPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharactersCount);
                statment.setInt(1, WorldSettings.getInt("world.id"));
                statment.setInt(2, session.getId());
                result = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getAuthPool().returnObject(current);
                }
            }
            int currCharCount = 0;
            int allCharCount = 0;
            while (result.next()) {
                if (result.getInt("realmid") == WorldSettings.getInt("world.id")) {
                    currCharCount = result.getInt("count");
                }
                allCharCount += result.getInt("count");
            }
            if (allCharCount >= 50 || currCharCount >= 11) {
                output.writeByte(CharacterCreateResult.ACCOUNT_LIMIT.getData());
                channel.write(output);
                return;
            }
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharacterCreationData);
                statment.setInt(1, race);
                statment.setInt(2, classid);
                result = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
            if (!result.next()) {
                output.writeUInt8(CharacterCreateResult.FAILED.getData());
            } else {
                int map = result.getInt("map");
                int zone = result.getInt("zone");
                float posX = result.getFloat("posX");
                float posY = result.getFloat("posY");
                float posZ = result.getFloat("posZ");
                float posO = result.getFloat("posO");
                int characterFlags = CharacterFlag.DECLINE.getData();
                dbConnection = MySqlConnection.getInstance();
                try {
                    current = dbConnection.getCharPool().borrowObject();
                    Connection connection = (Connection) current;
                    PreparedStatement statment;
                    statment = connection.prepareStatement(Query.createCharacter);
                    statment.setString(1, normalizedName);
                    statment.setInt(2, session.getId());
                    statment.setInt(3, race);
                    statment.setInt(4, classid);
                    statment.setInt(5, gender);
                    statment.setInt(6, skin);
                    statment.setInt(7, zone);
                    statment.setInt(8, map);
                    statment.setFloat(9, posX);
                    statment.setFloat(10, posY);
                    statment.setFloat(11, posZ);
                    statment.setFloat(12, posO);
                    statment.setInt(13, face);
                    statment.setInt(14, hairStyle);
                    statment.setInt(15, hairColor);
                    statment.setInt(16, facialHair);
                    statment.setInt(17, characterFlags);
                    statment.execute();
                } finally {
                    if (current != null) {
                        dbConnection.getCharPool().returnObject(current);
                    }
                }
                dbConnection = MySqlConnection.getInstance();
                try {
                    current = dbConnection.getAuthPool().borrowObject();
                    Connection connection = (Connection) current;
                    PreparedStatement statment;
                    statment = connection.prepareStatement(Query.incCharactersCount);
                    statment.setInt(1, WorldSettings.getInt("world.id"));
                    statment.setInt(2, session.getId());
                    statment.execute();
                } finally {
                    if (current != null) {
                        dbConnection.getAuthPool().returnObject(current);
                    }
                }
                output.writeByte(CharacterCreateResult.SUCCESS.getData());
            }
        }
        channel.write(output);
    }

    public static void deleteCharacter(WorldSession session, Packet input) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        boolean[] guidMask = new boolean[8];
        byte[] guidBytes = new byte[8];
        int[] guidIndex = {1, 3, 4, 0, 7, 2, 5, 6};
        BitUnpack bin = new BitUnpack(input);
        guidMask[2] = bin.read();
        guidMask[1] = bin.read();
        guidMask[5] = bin.read();
        guidMask[7] = bin.read();
        guidMask[6] = bin.read();
        boolean unknown = bin.read();
        guidMask[3] = bin.read();
        guidMask[0] = bin.read();
        guidMask[4] = bin.read();
        for (int i = 0; i < guidIndex.length; i++) {
            if (guidMask[guidIndex[i]]) {
                guidBytes[guidIndex[i]] = (byte) (input.readInt8() ^ 1);
            }
        }
        long guid = ArrayUtils.byteArrayToLong(guidBytes);
        Channel channel = session.getChannel();
        int deleted;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.deleteCharacter);
            statment.setLong(1, guid);
            statment.setInt(2, session.getId());
            deleted = statment.executeUpdate();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (deleted > 0) {
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.deleteCharacterTalents);
                statment.setLong(1, guid);
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getAuthPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.decCharactersCount);
                statment.setInt(1, WorldSettings.getInt("world.id"));
                statment.setInt(2, session.getId());
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getAuthPool().returnObject(current);
                }
            }
            Packet output = new Packet(5, ServerOpcode.DELETE_CHARACTER);
            output.writeUInt8(0x47);
            channel.write(output);
            enumCharactersResult(session);
        } else {
            channel.close();
        }
    }
    
    public static void playerLogin(WorldSession session, Packet input) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        byte[] guidMask = {2, 0, 4, 3, 5, 6, 1, 7};
        byte[] guidBytes = {0, 3, 7, 6, 1, 2, 4, 5};
        input.skipBytes(4);
        BitUnpack bits = new BitUnpack(input);
        long guid = bits.getPacketValue(guidMask, guidBytes);
        session.setPlayer(new Player(session, guid));
        if(!WorldMgr.addSession(session)) {
            session.getChannel().close();
            return;
        }
        session.setState(SessionState.IN_GAME);
        WorldMgr.writeAccountDataTimes(AccountDataMasks.CHARACTER_CACHE_MASK, session);
        MiscHandler.messageOfTheDay(session);
        TimeHandler.loginSetTimeSpeed(session);
        SpecializationHandler.updateTalentData(session);
        SpellHandler.sendKnownSpells(session);
        MiscHandler.updateActionButtons(session);
        if (session.getPlayer().isLoginCinematic()) {
            CinematicHandler.startCinematic(session);
        }
        ObjectHandler.updateObjectCreate(session);
    }
}
