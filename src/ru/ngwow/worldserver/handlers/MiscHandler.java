package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.ActionMgr;
import ru.ngwow.worldserver.objectdefines.ActionButton;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.uint.UInt32;
import ru.ngwow.worldserver.utils.array.ArrayUtils;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.bits.BitUnpack;

public class MiscHandler {
    
    private MiscHandler() {
    }
    
    public static void cacheVersion(WorldSession session) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(8, ServerOpcode.UPDATE_CLIENT_CACHE_VERSION);
        output.writeUInt32(0);
        channel.write(output);
    }
    
    public static void ping(WorldSession session, Packet packet) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (packet == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        int latency = packet.readInt32();
        int sequence = packet.readInt32();
        Packet output = new Packet(8, ServerOpcode.PONG);
        output.writeInt32(sequence);
        channel.write(output);
    }
    
    public static void disconnect(WorldSession session, Packet input) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (session == null) {
            throw new NullPointerException("packet is null");
        }
    }
    
    public static void messageOfTheDay(WorldSession session) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.MOTD);
        BitPack bout = new BitPack(output);
        String motd = WorldSettings.get("motd");
        bout.write(1, 4);
        bout.write(motd.length(), 7);
        bout.flush();
        output.writeString(motd);
        channel.write(output);
    }
    
    public static void updateActionButtons(WorldSession session) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Player player = session.getPlayer();
        
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.UPDATE_ACTION_BUTTONS);
        BitPack bout = new BitPack(output);
        int buttonCount = 132;
        byte[][] buttons = new byte[buttonCount][8];
        byte[] buttonMask = {1, 2, 0, 3, 6, 5, 4, 7};
        byte[] buttonBytes = {1, 7, 2, 5, 0, 3, 4, 6};
        for (int i = 0; i < buttonCount; i++) {
            int action = ActionMgr.getActionInSlot(player, i);
            if (action != 0) {
                buttons[i] = ArrayUtils.longToByteArray(action);
            }
        }
        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < buttonCount; j++) {
                if (i < 8) {
                    bout.write(buttons[j][buttonMask[i]] != 0 ? 1 : 0);
                } else if (i < 16) {
                    bout.flush();
                    if (buttons[j][buttonBytes[i - 8]] != 0)
                        output.writeUInt8((byte)(buttons[j][buttonBytes[i - 8]] ^ 1));
                    }
                }
            }
        output.writeInt8(0);
        channel.write(output);
    }
    
    public static void setActionButton(WorldSession session, Packet input)
            throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (session == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        Player player = session.getPlayer();
        byte[] actionMask = {2, 5, 0, 3, 6, 4, 1, 7};
        byte[] actionBytes = {7, 3, 0, 2, 1, 5, 4, 6};
        int slotId = input.readByte();
        BitUnpack bin = new BitUnpack(input);
        long actionId = bin.getPacketValue(actionMask, actionBytes);
        if (actionId == 0) {
            ActionButton actionButton = ActionMgr.getActionButtonInSlot(player, slotId);
            if (actionButton != null) {
                ActionMgr.removeActionButton(player, actionButton, true);
            }
        } else {
            ActionButton actionButton = new ActionButton((int)actionId, slotId, player.getActiveSpecGroup());
            ActionMgr.addActionButton(player, actionButton, true);
        }
    }
    
    public static void zoneUpdate(WorldSession session, Packet input)
            throws Exception {
        UInt32 zone = input.readUInt32();
        Player player = session.getPlayer();
        player.setZone(zone.intValue());
    }
    
    public static void loadingScreenNotify(WorldSession session, Packet input) {
    }
    
    public static void violenceLevel(WorldSession session, Packet input) {
    }
    
    public static void activePlayer(WorldSession session, Packet input) {
    }
}
