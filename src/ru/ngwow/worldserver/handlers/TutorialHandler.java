package ru.ngwow.worldserver.handlers;

import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;

public class TutorialHandler {
    
    private TutorialHandler() {
    }
    
    public static void tutorialFlags(WorldSession session) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(38, ServerOpcode.TUTORIAL_FLAGS);
        for (int i = 0; i < 8; i++) {
            output.writeUInt32(0);
        }
        channel.write(output);
    }
}
