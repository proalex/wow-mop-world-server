package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import java.util.ArrayList;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.clientdb.structures.DBCSpecializationSpells;
import ru.ngwow.worldserver.clientdb.structures.DBCTalent;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.Spell;
import ru.ngwow.worldserver.managers.SpecializationMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.bits.BitPack;

public class SpellHandler {
    
    private SpellHandler() {
    }
    
    public static void sendKnownSpells(WorldSession session) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Player player = session.getPlayer();
        Packet output = new Packet(ServerOpcode.SEND_KNOWN_SPELLS);
        BitPack bout = new BitPack(output);
        ArrayList<DBCSpecializationSpells> specializationSpells = 
                SpecializationMgr.getSpecializationSpells(player);
        int specializationSpellCount = specializationSpells != null ? specializationSpells.size() : 0;
        ArrayList<DBCTalent> talentSpells = 
                SpecializationMgr.getTalentSpells(player, player.getActiveSpecGroup());
        ArrayList<Spell> playerSpells = player.getSpells();
        int talentSpellCount = talentSpells != null ? talentSpells.size() : 0;
        int count = playerSpells.size() + specializationSpellCount + talentSpellCount;
        bout.write(count, 22);
        bout.write(1);
        bout.flush();
        for (int i = 0; i < playerSpells.size(); i++) {
            output.writeUInt32(playerSpells.get(i).getSpellId());
        }
        for (int i = 0; i < specializationSpellCount; i++) {
            output.writeUInt32(specializationSpells.get(i).spell);
        }
        for (int i = 0; i < talentSpellCount; i++) {
            output.writeUInt32(talentSpells.get(i).spellId);
        }
        channel.write(output);
    }
}
