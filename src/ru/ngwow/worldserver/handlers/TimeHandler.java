package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import java.nio.charset.Charset;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.AccountDataMasks;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.bits.BitPack;

public class TimeHandler {
    
    private TimeHandler() {
    }
    
    public static void realmSplit(WorldSession session, Packet input)
            throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        String date = "01/01/01";
        Packet output = new Packet(ServerOpcode.REALM_SPLIT);
        BitPack bout = new BitPack(output);
        bout.write(date.length(), 7);
        output.writeString(date);
        output.writeInt32(input.readInt32());
        output.writeUInt32(0);
        channel.write(output);
    }
    
    public static void readyForAccountDataTimes(WorldSession session) throws IOException {
        WorldMgr.writeAccountDataTimes(AccountDataMasks.GLOBAL_CACHE_MASK, session);
    }
    
    public static void loginSetTimeSpeed(WorldSession session) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.LOGIN_TIME_SPEED);
        output.writePackedTime();
        output.writePackedTime();
        output.writeInt32(1);
        output.writeInt32(1);
        output.writeFloat(0.01666667F);
        channel.write(output);
    }
    
    public static void uiTimeRequest(WorldSession session, Packet input) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.UI_TIME);
        output.writeUnixTime();
        channel.write(output);
    }
}
