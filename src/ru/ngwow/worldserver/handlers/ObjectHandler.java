package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.ObjectType;
import ru.ngwow.worldserver.constants.UpdateFlag;
import ru.ngwow.worldserver.constants.UpdateType;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.WorldObject;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.bits.BitPack;

public class ObjectHandler {
    
    private ObjectHandler() {
        
    }
    
    public static Packet destroyObject(WorldObject object, boolean animation)
            throws IOException {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        Packet packet = new Packet(ServerOpcode.DESTROY_OBJECT);
        BitPack bout = new BitPack(packet);
        bout.setGuid(object.getGuid());
        bout.writeGuidMask(4);
        bout.write(animation);
        bout.writeGuidMask(0, 1, 6, 2, 5, 7, 3);
        bout.flush();
        bout.writeGuidBytes(7, 1, 2, 5, 0, 3, 6, 4);
        return packet;
    }
    
    public static Packet destroyObject(WorldObject object)
            throws IOException {
        return destroyObject(object, false);
    }
    
    public static void updateObjectCreate(WorldSession session) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Player player = session.getPlayer();
        Packet output = new Packet(ServerOpcode.OBJECT_UPDATE);
        Channel channel = session.getChannel();
        output.writeUInt16(player.getMap());
        output.writeUInt32(1);
        output.writeUInt8(UpdateType.CREATE_OBJECT.getData());
        output.writeGuid(player.getGuid());
        output.writeUInt8(ObjectType.PLAYER.getData());
        int updateFlags = UpdateFlag.ALIVE.getData() | UpdateFlag.ROTATION.getData() | UpdateFlag.SELF.getData();
        WorldMgr.writeUpdateObjectMovement(output, player, updateFlags);
        player.setUpdateFields();
        player.writeUpdateFields(output);
        player.writeDynamicUppdateFields(output);
        channel.write(output);
    }
}
