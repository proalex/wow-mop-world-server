package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.MessageType;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.objectdefines.ChatMessageValues;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.uint.UInt8;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.bits.BitUnpack;

public class ChatHandler {
    
    private ChatHandler() {
    }
    
    public static void chatMessageSay(WorldSession session, Packet input) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("input is null");
        }
        BitUnpack bin = new BitUnpack(input);
        int language = input.readInt();
        UInt8 messageLength = input.readUInt8();
        String message = input.readString(messageLength.intValue());
        ChatMessageValues chatMessage = new ChatMessageValues(
                MessageType.CHAT_MESSAGE_SAY, message);
        chatMessage.setLanguage(language);
        chatMessage.setRealmId(WorldSettings.getInt("world.id"));
        sendMessage(session, chatMessage);
    }
    
    public static void chatMessageYell(WorldSession session, Packet input) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("input is null");
        }
        BitUnpack bin = new BitUnpack(input);
        int language = input.readInt();
        UInt8 messageLength = input.readUInt8();
        String message = input.readString(messageLength.intValue());
        ChatMessageValues chatMessage = new ChatMessageValues(
                MessageType.CHAT_MESSAGE_YELL, message);
        chatMessage.setLanguage(language);
        chatMessage.setRealmId(WorldSettings.getInt("world.id"));
        sendMessage(session, chatMessage);
    }
    
    /*public static void chatMessageWhisper(WorldSession session, Packet input) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("input is null");
        }
        BitUnpack bin = new BitUnpack(input);
        int language = input.readInt();
        int nameLength = bin.read(9);
        int messageLength = bin.read(8);
        String message = input.readString(messageLength);
        String receiverName = input.readString(nameLength);
        WorldSession rSession = WorldMgr.getSession(receiverName);
        if (rSession == null) {
            return;
        }
        if (rSession.getPlayer().getFaction() == session.getPlayer().getFaction()) {
            ChatMessageValues chatMessage = new ChatMessageValues(
                    MessageType.CHAT_MESSAGE_WHISPER_INFORM, message);
            chatMessage.setRealmId(WorldSettings.getInt("world.id"));
            sendMessage(session, chatMessage, rSession);
            chatMessage = new ChatMessageValues(
                    MessageType.CHAT_MESSAGE_WHISPER, message);
            chatMessage.setRealmId(WorldSettings.getInt("world.id"));
            sendMessage(rSession, chatMessage, session);
        }
    }*/
    
    public static void sendMessage(WorldSession session, ChatMessageValues chatMessage,
            WorldSession rSession) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (chatMessage == null) {
            throw new NullPointerException("chatMessage is null");
        }
        int[] guidMask = {5, 4, 1, 0, 6, 2, 7, 3};
        int[] guidMask2 = {1, 5, 7, 4, 2, 0, 6, 3};
        int[] guidBytes = {5, 0, 7, 4, 3, 2, 1, 6};
        int[] guidBytes2 = {7, 4, 2, 3, 1, 5, 6, 0};
        Player player = session.getPlayer();
        long guid;
        if (rSession != null) {
            guid = rSession.getPlayer().getGuid();
        } else {
            guid = player.getGuid();
        }
        Packet output = new Packet(ServerOpcode.CHAT);
        BitPack bout = new BitPack(output);
        bout.setGuid(guid);
        bout.write(1);
        bout.write(0);
        bout.write(1);
        bout.write(1);
        bout.write(0, 8);
        bout.write(0);
        bout.write(1);
        bout.writeGuidMask(guidMask2);
        bout.write(0);
        bout.write(1);
        bout.write(1);
        bout.write(0, 8);
        bout.write(0);
        bout.write(1);
        bout.writeStringLength(chatMessage.getMessage(), 12);
        bout.write(!chatMessage.hasRealmId());
        bout.write(1);
        bout.write(0);
        bout.writeGuidMask(guidMask);
        bout.write(0);
        bout.write(8, 9);
        bout.write(!chatMessage.hasLanguage());
        bout.flush();
        bout.writeGuidBytes(guidBytes2);
        bout.writeGuidBytes(guidBytes);
        output.writeUInt8(chatMessage.getType().getData());
        output.writeString(chatMessage.getMessage());
        if (chatMessage.hasRealmId()) {
            output.writeInt32(chatMessage.getRealmId());
        }
        if (chatMessage.hasLanguage()) {
            output.writeUInt8(chatMessage.getLanguage());
        }
        switch(chatMessage.getType()) {
            case CHAT_MESSAGE_SAY:
                WorldMgr.sendToInRangeCharacter(player, output, true);
                break;
            case CHAT_MESSAGE_YELL:
                WorldMgr.sendToInRangeCharacter(player, output, true);
                break;
            default:
                Channel channel = session.getChannel();
                channel.write(output);
                break;
        }
    }
    
    public static void sendMessage(WorldSession session, ChatMessageValues chatMessage)
            throws IOException {
        sendMessage(session, chatMessage, null);
    }
}
