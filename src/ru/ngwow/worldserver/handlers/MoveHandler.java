package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.objectdefines.ObjectMovementValues;
import ru.ngwow.worldserver.objectdefines.Vector4;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.uint.UInt32;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;
import ru.ngwow.worldserver.utils.array.ArrayUtils;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.bits.BitUnpack;

public class MoveHandler {
    
    private MoveHandler() {
        
    }
    
    public static void move(WorldSession session, Packet input) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Player player = session.getPlayer();
        Vector4 position = new Vector4();
        ObjectMovementValues movementValues = new ObjectMovementValues();
        BitUnpack bin = new BitUnpack(input);
        boolean[] guidMask = new boolean[8];
        byte[] guidBytes = new byte[8];
        position.y = input.readFloat();
        position.z = input.readFloat();
        position.x = input.readFloat();
        position.o = 0;
        guidMask[3] = bin.read();
        boolean hasPitch = !bin.read();
        guidMask[0] = bin.read();
        UInt32 counter = uint32(bin.read(22));
        guidMask[2] = bin.read();
        boolean hasSplineElevation = !bin.read();
        movementValues.hasRotation = !bin.read();
        boolean unknown = bin.read();
        boolean unknown2 = bin.read();
        guidMask[7] = bin.read();
        boolean hasTime = !bin.read();
        movementValues.isFallingOrJumping = bin.read();
        movementValues.hasMovementFlags2 = !bin.read();
        movementValues.hasMovementFlags = !bin.read();
        boolean unknown3 = !bin.read();
        boolean unknown4 = bin.read();
        guidMask[6] = bin.read();
        guidMask[1] = bin.read();
        movementValues.isTransport = bin.read();
        guidMask[4] = bin.read();
        guidMask[5] = bin.read();
        if (movementValues.hasMovementFlags) {
            movementValues.movementFlags = bin.read((30));
        }
        if (movementValues.isFallingOrJumping) {
            movementValues.hasJumpData = bin.read();
        }
        if (movementValues.hasMovementFlags2) {
            movementValues.movementFlags2 = bin.read((13));
        }
        if (guidMask[0]) {
            guidBytes[0] = (byte) (input.readByte() ^ 1);
        }
        for (int i = 0; i < counter.longValue(); i++) {
            input.readUInt32();
        }
        if (guidMask[4]) {
            guidBytes[4] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[1]) {
            guidBytes[1] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[5]) {
            guidBytes[5] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[6]) {
            guidBytes[6] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[2]) {
            guidBytes[2] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[3]) {
            guidBytes[3] = (byte) (input.readByte() ^ 1);
        }
        if (guidMask[7]) {
            guidBytes[7] = (byte) (input.readByte() ^ 1);
        }
        if (hasPitch) {
            input.readFloat();
        }
        if (movementValues.hasRotation) {
            position.o = input.readFloat();
        }
        if (movementValues.isFallingOrJumping) {
            movementValues.jumpVelocity = input.readFloat();
            if (movementValues.hasJumpData) {
                movementValues.currentSpeed = input.readFloat();
                movementValues.sin = input.readFloat();
                movementValues.cos = input.readFloat();
            }
            movementValues.fallTime = input.readUInt32();
        }
        if (unknown3) {
            input.readUInt32();
        }
        if (hasSplineElevation) {
            input.readFloat();
        }
        if (hasTime) {
            movementValues.time = input.readUInt32();
        }
        long guid = ArrayUtils.byteArrayToLong(guidBytes);
        if (player.getGuid() != guid) {
            session.getChannel().close();
        }
        moveUpdate(player, movementValues, position);
     }
    
    public static void moveUpdate(Player player, ObjectMovementValues movementValues,
            Vector4 position) throws Exception {
        if (movementValues == null) {
            throw new NullPointerException("movementValues is null");
        }
        Packet output = new Packet(ServerOpcode.MOVE_UPDATE);
        BitPack bout = new BitPack(output);
        bout.setGuid(player.getGuid());
        bout.writeGuidMask(0);
        bout.write(!movementValues.hasRotation);
        bout.write(!movementValues.hasMovementFlags2);
        bout.writeGuidMask(5);
        if (movementValues.hasMovementFlags2) {
            bout.write(movementValues.movementFlags2, 13);
        }
        bout.write(!movementValues.hasMovementFlags);
        bout.write(0);
        bout.write(0, 22);
        bout.write(1);
        bout.write(1);
        bout.write(0);
        bout.write(0);
        bout.writeGuidMask(7);
        bout.write(movementValues.isTransport);
        bout.write(1);
        bout.writeGuidMask(4, 1);
        bout.write(movementValues.isFallingOrJumping);
        if (movementValues.hasMovementFlags) {
            bout.write(movementValues.movementFlags, 30);
        }
        bout.write(movementValues.time.longValue() == 0);
        if (movementValues.isFallingOrJumping) {
            bout.write(movementValues.hasJumpData);
        }
        bout.writeGuidMask(2, 3, 6);
        bout.flush();
        bout.writeGuidBytes(6);
        if (movementValues.time.longValue() != 0) {
            output.writeUInt32(movementValues.time.longValue());
        }
        if (movementValues.isFallingOrJumping) {
            output.writeFloat(movementValues.jumpVelocity);
            if (movementValues.hasJumpData) {
                output.writeFloat(movementValues.cos);
                output.writeFloat(movementValues.currentSpeed);
                output.writeFloat(movementValues.sin);
            }
            output.writeUInt32(movementValues.fallTime);
        }
        output.writeFloat(position.x);
        bout.writeGuidBytes(1);
        output.writeFloat(position.y);
        bout.writeGuidBytes(2, 7, 5);
        output.writeFloat(position.z);
        bout.writeGuidBytes(0, 4, 3);
        if (movementValues.hasRotation) {
            output.writeFloat(position.o);
        }
        player.setPosition(position);
        WorldMgr.sendToInRangeCharacter(player, output);
    }
}
