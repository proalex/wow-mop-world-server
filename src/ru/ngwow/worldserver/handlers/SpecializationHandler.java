package ru.ngwow.worldserver.handlers;

import java.io.IOException;
import java.util.ArrayList;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.SpecializationMgr;
import ru.ngwow.worldserver.objectdefines.Talent;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.bits.BitPack;

public class SpecializationHandler {
    
    private SpecializationHandler() {
    }
    
    public static void updateTalentData(WorldSession session) throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        int glyphCount = 6;
        Player player = session.getPlayer();
        Packet packet = new Packet(ServerOpcode.UPDATE_TALENT_DATA);
        BitPack bout = new BitPack(packet);
        bout.write(player.getSpecGroupCount(), 19);
        for (int i = 0; i < player.getSpecGroupCount(); i++) {
            ArrayList<Talent> talents = SpecializationMgr.getTalentsBySpecGroup(player, i);
            bout.write(talents.size(), 23);
        }
        bout.flush();
        for (int i = 0; i < player.getSpecGroupCount(); i++) {
            ArrayList<Talent> talents = SpecializationMgr.getTalentsBySpecGroup(player, i);
            int specId = (i == 0) ? player.getPrimarySpec() : player.getSecondarySpec();
            for (int j = 0; j < glyphCount; j++) {
                packet.writeInt16(0);
            }
            for (int j = 0; j < talents.size(); j++) {
                packet.writeUInt16(talents.get(j).getId());
            }
            packet.writeUInt32(specId);
        }
        packet.writeUInt8(player.getActiveSpecGroup());
        channel.write(packet);
    }
}
