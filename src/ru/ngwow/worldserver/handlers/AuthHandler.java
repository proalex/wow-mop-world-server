package ru.ngwow.worldserver.handlers;

import java.util.Calendar;
import java.util.Random;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.AuthCodes;
import ru.ngwow.worldserver.database.data.ClassExpansion;
import ru.ngwow.worldserver.database.data.RaceExpansion;
import ru.ngwow.worldserver.database.loader.DBLoader;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.bits.BitPack;

public class AuthHandler {

    private static final Logger log = Logger.getLogger(AuthHandler.class.getName());
    private static Random random = new Random();
    private static byte[] hello = {0x52, 0x4c, 0x44, 0x20,
        0x4f, 0x46, 0x20, 0x57, 0x41, 0x52, 0x43, 0x52,
        0x41, 0x46, 0x54, 0x20, 0x43, 0x4f, 0x4e, 0x4e,
        0x45, 0x43, 0x54, 0x49, 0x4f, 0x4e, 0x20, 0x2d,
        0x20, 0x53, 0x45, 0x52, 0x56, 0x45, 0x52, 0x20,
        0x54, 0x4f, 0x20, 0x43, 0x4c, 0x49, 0x45, 0x4e,
        0x54, 0x00};
    
    private AuthHandler() {
    }

    public static void initConnection(WorldSession session) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(hello.length + 4, ServerOpcode.HELLO);
        output.writeBytes(hello);
        channel.write(output);
    }

    public static void sendAuthChallenge(WorldSession session) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(43, ServerOpcode.AUTH_CHALLENGE);
        output.writeUInt8(0);
        output.writeUInt8(0);
        for (int i = 0; i < 8; i++) {
            output.writeUInt32(0);
        }
        long rand = new Random(Calendar.getInstance().get(Calendar.SECOND)).nextLong();
        output.writeUInt32(rand);
        output.writeUInt8(1);
        channel.write(output);
    }

    public static void authSession(WorldSession session, Packet input) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            throw new NullPointerException("packet is null");
        }
        Channel channel = session.getChannel();
        input.skipBytes(54);
        int addonSize = input.readInt32();
        if (addonSize < 1 || addonSize > 2048 || addonSize >= input.readableBytes()) {
            channel.close();
            return;
        }
        input.skipBytes(addonSize + 2);
        if (input.readableBytes() > 128) {
            channel.close();
            return;
        }
        String email = input.readString(input.readableBytes());
        if (!session.setEmail(email)) {
            channel.close();
            return;
        }
        byte[] k = session.getSessionKey().asByteArray();
        if (k == null || k.length != 40) {
            channel.close();
            return;
        }
        if (session.isBanned()) {
            channel.close();
            return;
        }
        if (session.isAuthed()) {
            channel.close();
            return;
        }
        session.getCrypt().init(k);
        Packet output = new Packet(ServerOpcode.AUTH_RESPONSE);
        BitPack bout = new BitPack(output);
        bout.write(0);
        bout.write(1);
        bout.write(0);
        bout.write(0);
        bout.write(DBLoader.getRaceData().size(), 23);
        bout.write(0);
        bout.write(0, 21);
        bout.write(DBLoader.getClassData().size(), 23);
        bout.write(0, 22);
        bout.write(0);
        bout.flush();
        output.writeUInt8(0);
        for (int i = 0; i < DBLoader.getClassData().size(); i++) {
            ClassExpansion cl = (ClassExpansion) DBLoader.getClassData().get(i);
            output.writeUInt8(cl.getExpansion());
            output.writeUInt8(cl.getClassId());
        }
        for (int i = 0; i < DBLoader.getRaceData().size(); i++) {
            RaceExpansion rl = (RaceExpansion) DBLoader.getRaceData().get(i);
            output.writeUInt8(rl.getExpansion());
            output.writeUInt8(rl.getRace());
        }
        output.writeUInt32(0);
        output.writeUInt32(0);
        output.writeUInt32(0);
        output.writeUInt8(session.getExpansion());
        output.writeUInt8(session.getExpansion());
        output.writeUInt8(AuthCodes.AUTH_OK.getData());
        channel.write(output);
        MiscHandler.cacheVersion(session);
        TutorialHandler.tutorialFlags(session);
    }
}
