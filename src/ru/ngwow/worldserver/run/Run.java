package ru.ngwow.worldserver.run;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import org.jboss.netty.handler.execution.OrderedMemoryAwareThreadPoolExecutor;
import ru.ngwow.worldserver.clientdb.loader.DBCLoader;
import ru.ngwow.worldserver.database.loader.DBLoader;
import ru.ngwow.worldserver.database.query.DBHelper;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.network.PacketQueue;
import ru.ngwow.worldserver.network.ServerPipelineFactory;
import ru.ngwow.worldserver.settings.WorldSettings;

public class Run {

    private static final Logger log = Logger.getLogger(Run.class.getName());
    public static boolean running = true;

    public static void main(String[] args) throws IOException {
        ServerBootstrap bootstrap = null;
        Channel bind = null;
        try {
            ChannelFactory factory;
            WorldSettings.load();
            ExecutorService bossExecutor = new OrderedMemoryAwareThreadPoolExecutor
                    (WorldSettings.getInt("thread.acceptor"), 1048576, 1048576);
            ExecutorService ioExecutor = new OrderedMemoryAwareThreadPoolExecutor
                    (WorldSettings.getInt("thread.io"), 1048576, 1048576);
            factory = new NioServerSocketChannelFactory
                    (bossExecutor, ioExecutor, WorldSettings.getInt("thread.io"));
            bootstrap = new ServerBootstrap(factory);
            bootstrap.setPipelineFactory(new ServerPipelineFactory());
            bootstrap.setOption("child.tcpNoDelay", true);
            bootstrap.setOption("child.keepAlive", true);
            DBCLoader.load();
            DBLoader.loadData();
            DBHelper.updateOnline();
            log.log(Level.INFO, "Start listening {0} on {1} port...",
                    new Object[]{
                        WorldSettings.get("network.ip"),
                        WorldSettings.get("network.port")
                    });
            bind = bootstrap.bind(
                    new InetSocketAddress(
                    WorldSettings.get("network.ip"),
                    WorldSettings.getInt("network.port")));
            new Thread(new PacketQueue(), "PacketQueue").start();
            WorldMgr.worldUpdate();
        } catch (Exception ex) {
            running = false;
            log.log(Level.INFO, "{0} Exception in {1} detected: {2}.",
                new Object[]{
                    new Date(),
                    Run.class.getName(),
                    ex.getMessage()
                });
            if (bind != null) {
                bind.close();
            }
            if (bootstrap != null) {
                bootstrap.shutdown();
            }
            System.exit(0);
        }
    }
}
