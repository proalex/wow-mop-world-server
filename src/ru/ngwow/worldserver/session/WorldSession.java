package ru.ngwow.worldserver.session;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.SessionState;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.encryption.Crypt;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.managers.WorldMgr;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.bignumber.BigNumber;

public final class WorldSession {

    private Channel channel;
    private Crypt crypt;
    private String email;
    private int id;
    private BigNumber sessionKey;
    private static final Logger log = Logger.getLogger(WorldSession.class.getName());
    private boolean online;
    private int expansion = 4;
    private Player player;
    private SessionState state = SessionState.NOT_IN_GAME;

    public WorldSession(Channel channel) {
        if (channel == null) {
            throw new NullPointerException("channel is null");
        }
        this.channel = channel;
        crypt = new Crypt();
    }

    public Channel getChannel() {
        return channel;
    }

    public Crypt getCrypt() {
        return crypt;
    }

    public BigNumber getSessionKey() {
        if (sessionKey == null) {
            throw new NullPointerException("sessionKey is null");
        }
        return sessionKey;
    }

    public boolean isBanned() throws SQLException, Exception {
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getBannedState);
            statment.setInt(1, id);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
        if (!result.next()) {
            throw new SQLException("failed to get banned state");
        }
        if (result.getInt("COUNT(id)") > 0) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isAuthed() {
        return online;
    }

    public boolean setEmail(String email) throws SQLException, Exception {
        if (email == null) {
            throw new NullPointerException("email is null");
        } else if (this.email != null) {
            return false;
        }
        this.email = email.toUpperCase();
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getAccountInfo);
            statment.setString(1, email);
            result = statment.executeQuery();
            if (!result.next()) {
                return false;
            }
            id = result.getInt("id");
            sessionKey = new BigNumber(result.getString("sessionkey").toUpperCase());
            online = result.getInt("online") > 0 ? true : false;
            expansion = result.getShort("expansion");
            return true;
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
    }

    public void setOnline(boolean state) throws SQLException, Exception {
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getAuthPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.setOnlineState);
            if (state == false) {
                statment.setInt(1, 0);
                statment.setInt(2, id);
            } else {
                statment.setInt(1, WorldSettings.getInt("world.id"));
                statment.setInt(2, id);
            }
            statment.execute();
            online = state;
        } finally {
            if (current != null) {
                dbConnection.getAuthPool().returnObject(current);
            }
        }
    }
    
    public void setPlayer(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        this.player = player;
    }
    
    public int getExpansion() {
        return expansion;
    }
    
    public int getId() {
        return id;
    }
    
    public Player getPlayer() {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        return player;
    }
    
    public SessionState getState() {
        return state;
    }
    
    public void setState(SessionState state) {
        this.state = state;
    }

    public void close() {
        if (isAuthed()) {
            try {
                WorldMgr.removeSession(this);
                setOnline(false);
                if (channel.isOpen()) {
                    channel.close();
                }
            } catch (Exception ex) {
                
            }
        }
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (!(object instanceof WorldSession)) {
            return false;
        }
        WorldSession session = (WorldSession) object;
        return id == session.id ? true : false;
    }
}
