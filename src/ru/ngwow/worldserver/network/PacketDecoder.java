package ru.ngwow.worldserver.network;

import java.nio.ByteOrder;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.utils.array.ArrayUtils;

public class PacketDecoder extends FrameDecoder {

    private static final Logger log = Logger.getLogger(PacketDecoder.class.getName());

    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
        WorldSession session = (WorldSession) channel.getAttachment();
        if (buffer.readableBytes() < 4) {
            return null;
        }  
        byte[] data = new byte[4];
        byte[] data2 = new byte[4];
        buffer.markReaderIndex();
        buffer.readBytes(data);
        data2[0] = data[0];
        data2[1] = data[1];
        data2[2] = data[2];
        data2[3] = data[3];
        if (session.getCrypt().isEnabled()) {
            data = session.getCrypt().decrypt(data);
        }
        ChannelBuffer header;
        int size = 0;
        int opcode;
        if (session.getCrypt().isEnabled()) {
            int cryptedHeader = ArrayUtils.byteArrayToInt(data);
            size = cryptedHeader >> 13;
            opcode = cryptedHeader & 0x1FFF;
        } else {
            header = ChannelBuffers.wrappedBuffer(ByteOrder.LITTLE_ENDIAN, data);
            size = header.readUnsignedShort() - 2;
            opcode = header.readUnsignedShort();
        }
        if ((size < 0) || (size > 2048)) {
            channel.close();
            return null;
        }
        if (buffer.readableBytes() < size) {
            buffer.resetReaderIndex();
            if (session.getCrypt().isEnabled()) {
                session.getCrypt().setPreviousState();
            }
            return null;
        }
        Packet packet = new Packet(size + 4);
        packet.writeInt(opcode);
        packet.writeBytes(buffer.readBytes(size));
        return packet;
    }
}
