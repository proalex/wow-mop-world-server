package ru.ngwow.worldserver.network;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.MessageEvent;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.run.Run;
import ru.ngwow.worldserver.settings.WorldSettings;

public class PacketQueue extends Thread {
    
    private static Queue<MessageEvent> queue = new ConcurrentLinkedQueue<>();
    public static ExecutorService packetHandler = 
            Executors.newFixedThreadPool(WorldSettings.getInt("thread.packethandler"));
    
    public static void add(MessageEvent e) {
        if (e == null) {
            throw new NullPointerException("e is null");
        }
        queue.add(e);
    }

    @Override
    public void run() {
        while(Run.running) {
            MessageEvent e = queue.poll();
            if (e != null) {
                Packet input = (Packet) e.getMessage();
                Channel channel = (Channel) e.getChannel();
                Runnable handler = new PacketHandler(input, channel);
                packetHandler.execute(handler);
            } else {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                }
            }
        }
    }
}
