package ru.ngwow.worldserver.network;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.channel.SimpleChannelHandler;
import ru.ngwow.worldserver.session.WorldSession;

public class ChannelHandler extends SimpleChannelHandler {

    @Override
    public void channelOpen(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        if (ctx == null) {
            throw new NullPointerException("ctx is null");
        }
        if (e == null) {
            throw new NullPointerException("e is null");
        }
        Channel channel = ctx.getChannel();
        WorldSession session = new WorldSession(channel);
        channel.setAttachment(session);
        Runnable handler = new PacketHandler(channel, false);
        PacketQueue.packetHandler.execute(handler);
    }

    @Override
    public void channelClosed(ChannelHandlerContext ctx, ChannelStateEvent e)
            throws Exception {
        if (ctx == null) {
            throw new NullPointerException("ctx is null");
        }
        if (e == null) {
            throw new NullPointerException("e is null");
        }
        Channel channel = ctx.getChannel();
        Runnable handler = new PacketHandler(channel, true);
        PacketQueue.packetHandler.execute(handler);
    }

    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e)
            throws Exception {
        if (ctx == null) {
            throw new NullPointerException("ctx is null");
        }
        if (e == null) {
            throw new NullPointerException("e is null");
        }
        PacketQueue.add(e);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e)
            throws Exception {
        if (ctx == null) {
            throw new NullPointerException("ctx is null");
        }
        if (e == null) {
            throw new NullPointerException("e is null");
        }
        Channel channel = ctx.getChannel();
        channel.close();
    }
}
