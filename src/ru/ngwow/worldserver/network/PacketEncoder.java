package ru.ngwow.worldserver.network;

import java.nio.ByteOrder;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.array.ArrayUtils;

public class PacketEncoder extends OneToOneEncoder {

    @Override
    protected Object encode(ChannelHandlerContext ctx, Channel channel, Object msg) throws Exception {
        synchronized(channel) {
            WorldSession session = (WorldSession) channel.getAttachment();
            ChannelBuffer output = ((ChannelBuffer) msg);
            int opcode = output.readInt();
            int size = output.readableBytes();
            if (WorldSettings.getInt("debug.packets") == 1) {
                ServerOpcode opc = ServerOpcode.getOpcode(opcode);
                if (opc.getData() != ServerOpcode.HELLO.getData()) {
                    String packetInfo = "ServerMessage: account " + session.getId()
                            + " size " + size;
                    packetInfo += " opcode " + opc.name() + "\n";
                    for (int i = 4; i < size + 4; i++) {
                        if((float)i / 100.0f == (int)i / 100) {
                            packetInfo += "\n";
                        }
                        packetInfo += String.format("%02X ", output.array()[i]);
                    }
                    packetInfo += "\n";
                    System.out.printf(packetInfo);
                }
            }
            byte[] header;
            if (session.getCrypt().isEnabled()) {
                int cryptedHeader = 0;
                cryptedHeader |= size & 0xFFFF;
                cryptedHeader <<= 13;
                cryptedHeader |= opcode & 0x1FFF;
                header = ArrayUtils.intToByteArray(cryptedHeader);
                ChannelBuffer frame = ChannelBuffers.buffer(ByteOrder.LITTLE_ENDIAN, size + 4);
                frame.writeBytes(session.getCrypt().encrypt(header));
                frame.writeBytes(output);
                return frame;
            } else {
                size += 2;
                header = new byte[4];
                header[0] = (byte) (0xFF & size);
                header[1] = (byte) (0xFF & size >>> 8);
                header[2] = (byte) (0xFF & opcode);
                header[3] = (byte) (0xFF & opcode >>> 8);
                ChannelBuffer frame = ChannelBuffers.buffer(ByteOrder.LITTLE_ENDIAN, size + 2);
                frame.writeBytes(header);
                frame.writeBytes(output);
                return frame;
            }
        }
    }
}