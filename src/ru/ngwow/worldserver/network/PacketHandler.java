package ru.ngwow.worldserver.network;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.exceptions.NoSuchOpcodeException;
import ru.ngwow.worldserver.handlers.AuthHandler;
import ru.ngwow.worldserver.opcodes.ClientOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;

public class PacketHandler implements Runnable {

    private Packet input;
    private Channel channel;
    private static final Logger log = Logger.getLogger(PacketHandler.class.getName());
    private boolean disconnected = false;
    
    public PacketHandler(Packet input, Channel channel) {
        if (channel == null) {
            throw new NullPointerException("channel is null");
        }
        this.input = input;
        this.channel = channel;
    }
    
    public PacketHandler(Channel channel, boolean disconnected) {
        if (channel == null) {
            throw new NullPointerException("channel is null");
        }
        this.channel = channel;
        this.disconnected = disconnected;
    }
    
    @Override
    public void run() {
        if (!channel.isConnected() && !disconnected) {
            return;
        }
        WorldSession session = (WorldSession) channel.getAttachment();
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (input == null) {
            if (disconnected) {
                session.close();
                return;
            } else {
                AuthHandler.initConnection(session);
                return;
            }
        }
        int data = input.readInt32();
        try {
            ClientOpcode opcode = ClientOpcode.getOpcode(data);
            if (WorldSettings.getInt("debug.packets") == 1) {
                String packetInfo = "ClientMessage: account " + session.getId()
                        + " size " + (input.array().length-4);
                packetInfo += " opcode " + opcode.name() + "\n";
                for (int i = 4; i < input.array().length; i++) {
                    if((float)i / 100.0f == (int)i / 100) {
                        packetInfo += "\n";
                    }
                    packetInfo += String.format("%02X ", input.array()[i]);
                }
                packetInfo += "\n";
                System.out.printf(packetInfo);
            }
            if (!opcode.equals(ClientOpcode.HELLO) && !opcode.equals(ClientOpcode.AUTH_SESSION) && !session.getCrypt().isEnabled() ||
                    !ClientOpcode.checkState(opcode, session)) {
                return;
            }
            opcode.handle(session, input);
        } catch (NoSuchOpcodeException ex) {
            if (session.getCrypt().isEnabled()) {
                if (WorldSettings.getInt("debug.unknownopcodes") == 1) {
                    log.log(Level.INFO, "{0} Received unknown opcode: {1}", new Object[]{
                               new Date(), String.format("0x%X", data)
                           });
                }
            } else {
                channel.close();
            }
        }
    }
}
