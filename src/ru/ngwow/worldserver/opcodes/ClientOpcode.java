package ru.ngwow.worldserver.opcodes;

import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.worldserver.constants.SessionState;
import ru.ngwow.worldserver.exceptions.NoSuchOpcodeException;
import ru.ngwow.worldserver.handlers.AuthHandler;
import ru.ngwow.worldserver.handlers.CacheHandler;
import ru.ngwow.worldserver.handlers.CharacterHandler;
import ru.ngwow.worldserver.handlers.ChatHandler;
import ru.ngwow.worldserver.handlers.LogoutHandler;
import ru.ngwow.worldserver.handlers.MiscHandler;
import ru.ngwow.worldserver.handlers.MoveHandler;
import ru.ngwow.worldserver.handlers.TimeHandler;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.session.WorldSession;

public enum ClientOpcode {

    AUTH_SESSION(0x09F1, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                AuthHandler.authSession(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), AuthHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    HELLO(0x4F57, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            AuthHandler.sendAuthChallenge(session);
        }
    },
    PING(0x08E3, SessionState.ANY) {
        @Override
        public void handle(WorldSession session, Packet input) {
            MiscHandler.ping(session, input);
        }
    },
    REALM_SPLIT(0x0B48, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                TimeHandler.realmSplit(session, input);
            } catch (Exception ex) {
                 log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), TimeHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    LOG_DISCONNECT(0x09A2, SessionState.ANY) {
        @Override
        public void handle(WorldSession session, Packet input) {
            MiscHandler.disconnect(session, input);
        }
    },
    ENUM_CHARACTERS(0x0B1D, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CharacterHandler.enumCharactersResult(session);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CharacterHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    READY_FOR_ACCOUNT_DATA_TIMES(0x0755, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                TimeHandler.readyForAccountDataTimes(session);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), TimeHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    CREATE_CHARACTER(0x0404, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CharacterHandler.createCharacter(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CharacterHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    DELETE_CHARACTER(0x010C, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CharacterHandler.deleteCharacter(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CharacterHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    PLAYER_LOGIN(0x0A19, SessionState.NOT_IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CharacterHandler.playerLogin(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CharacterHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    QUERY_PLAYER_NAME(0x0018, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CacheHandler.queryPlayerName(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CacheHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    SET_ACTION_BUTTON(0x0400, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MiscHandler.setActionButton(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MiscHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    UI_TIME_REQUEST(0x0405, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                TimeHandler.uiTimeRequest(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), TimeHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_FORWARD(0x0A4B, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_BACKWARD(0x08D2, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_STOP(0x0813, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_JUMP(0x0CCA, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_TURN_LEFT(0x0A9E, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_TURN_RIGHT(0x0A07, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_STOP_TURN(0x080A, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_FALL_LAND(0x0CCF, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_HEARTBEAT(0x0E0B, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_STRAFE_LEFT(0x0816, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_STRAFE_RIGHT(0x0843, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_STOP_STRAFE(0x0A4A, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_PITCH_UP(0x0C02, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_PITCH_DOWN(0x0A12, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_STOP_PITCH(0x0AC7, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_SET_RUN_MODE(0x081F, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_SET_WALK_MODE(0x0856, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_TOGGLE_COLLISION_CHEAT(0x0A5F, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_SET_FACING(0x0C43, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_SET_PITCH(0x0886, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_FALL_RESET(0x0AC3, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_SET_FLY(0x0ADF, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_ASCENT(0x089F, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_STOP_ASCENT(0x0A47, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_CHANGE_TRANSPORT(0x0C92, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_START_DESCEND(0x0893, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    MOVE_DISMISS_VEHICLE(0x0A56, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MoveHandler.move(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MoveHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    LOGOUT_REQUEST(0x1150, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                LogoutHandler.logoutRequest(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), LogoutHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    LOADING_SCREEN_NOTIFY(0x0341, SessionState.ANY) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MiscHandler.loadingScreenNotify(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MiscHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    VIOLENCE_LEVEL(0x054B, SessionState.ANY) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MiscHandler.violenceLevel(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MiscHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    ACTIVE_PLAYER(0x0704, SessionState.ANY) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MiscHandler.activePlayer(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MiscHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    QUERY_REALM_NAME(0x0209, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                CacheHandler.queryRealmName(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), CacheHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    ZONE_UPDATE(0x2000, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                MiscHandler.zoneUpdate(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), MiscHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    CHAT_MESSAGE_SAY(0x016A, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                ChatHandler.chatMessageSay(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), ChatHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    CHAT_MESSAGE_YELL(0x1333, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                ChatHandler.chatMessageYell(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), ChatHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    },
    /*CHAT_MESSAGE_WHISPER(0x143A, SessionState.IN_GAME) {
        @Override
        public void handle(WorldSession session, Packet input) {
            try {
                ChatHandler.chatMessageWhisper(session, input);
            } catch (Exception ex) {
                log.log(Level.SEVERE, "{0} Exception in {1} detected: {2}.", new Object[]{
                            new Date(), ChatHandler.class.getName(), ex.getMessage()});
                session.getChannel().close();
            }
        }
    }*/;
    
    private int id;
    private SessionState state;
    private static final Logger log = Logger.getLogger(ClientOpcode.class.getName());

    ClientOpcode(int opcode, SessionState state) {
        id = opcode;
        this.state = state;
    }

    public int getData() {
        return id;
    }
    
    public static boolean checkState(ClientOpcode opcode, WorldSession session) {
        if (opcode == null) {
            throw new NullPointerException("opcode is null");
        }
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        return (opcode.state == session.getState()
                || opcode.state == SessionState.ANY);
    }

    public static ClientOpcode getOpcode(int data) throws NoSuchOpcodeException {
        for (ClientOpcode current : ClientOpcode.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchOpcodeException();
    }

    public abstract void handle(WorldSession session, Packet input);
    
    public boolean equals(ClientOpcode opcode) {
        if (id == opcode.getData()) {
            return true;
        }
        return false;
    }
}