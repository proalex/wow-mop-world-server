package ru.ngwow.worldserver.opcodes;

import ru.ngwow.worldserver.exceptions.NoSuchOpcodeException;

public enum ServerOpcode {

    AUTH_CHALLENGE(0x0221),
    AUTH_RESPONSE(0x0890),
    HELLO(0x4F57),
    UPDATE_CLIENT_CACHE_VERSION(0x1489),
    TUTORIAL_FLAGS(0x0D7E),
    PONG(0x1121),
    REALM_SPLIT(0x0F89),
    ENUM_CHARACTERS_RESULT(0x0FDD),
    ACCOUNT_DATA_TIMES(0x0CD1),
    CREATE_CHARACTER(0x1495),
    DELETE_CHARACTER(0x14C1),
    MOTD(0x12DC),
    LOGIN_TIME_SPEED(0x0C8D),
    UPDATE_TALENT_DATA(0x09D1),
    SEND_KNOWN_SPELLS(0x173F),
    UPDATE_ACTION_BUTTONS(0x019C),
    OBJECT_UPDATE(0x0C65),
    START_CINEMATIC(0x15BE),
    QUERY_PLAYER_NAME_RESPONSE(0x0BD0),
    UI_TIME(0x14C9),
    MOVE_UPDATE(0x1109),
    LOGOUT_COMPLETE(0x15C9),
    DESTROY_OBJECT(0x04D1),
    REALM_QUERY_RESPONCE(0x10CC),
    CHAT(0x0699);
    
    private int id;

    ServerOpcode(int opcode) {
        id = opcode;
    }

    public int getData() {
        return id;
    }
    
    public static ServerOpcode getOpcode(int data) throws NoSuchOpcodeException {
        for (ServerOpcode current : ServerOpcode.values()) {
            if (data == current.getData()) {
                return current;
            }
        }
        throw new NoSuchOpcodeException();
    }
}
