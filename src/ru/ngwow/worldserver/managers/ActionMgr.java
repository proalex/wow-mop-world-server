package ru.ngwow.worldserver.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.objectdefines.ActionButton;

public class ActionMgr {
    
    public static void loadActionButtons(Player player) throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Object current = null;
        ResultSet result;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacterActions);
            statment.setLong(1, player.getGuid());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (!result.next()) {
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharacterCreationActions);
                statment.setInt(1, player.getRace());
                statment.setInt(2, player.getClassid());
                result = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
            while (result.next()) {
                ActionButton actionButton = new ActionButton(
                    result.getInt("action"), result.getInt("slotId"), player.getActiveSpecGroup());
                addActionButton(player, actionButton, true);
            }
        } else {
            do {
                ActionButton actionButton = new ActionButton(
                        result.getInt("action"), result.getInt("slotId"), result.getInt("specGroup"));
                addActionButton(player, actionButton);

            } while (result.next());
        }
    }
    
    public static void addActionButton(Player player, ActionButton actionButton, boolean addToDB)
            throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        if (actionButton == null) {
            throw new NullPointerException("actionButton is null");
        }
        player.getActionButtons().add(actionButton);
        if (addToDB) {
            Object current = null;
            MySqlConnection dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.addActionButton);
                statment.setLong(1, player.getGuid());
                statment.setInt(2, actionButton.getAction());
                statment.setInt(3, actionButton.getSlotId());
                statment.setInt(4, actionButton.getSpecGroup());
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
        }
    }
    
    public static void addActionButton(Player player, ActionButton actionButton) throws Exception {
        addActionButton(player, actionButton, false);
    }
    
    public static void removeActionButton(Player player, ActionButton actionButton, boolean deleteFromDb)
            throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        if (actionButton == null) {
            throw new NullPointerException("actionButton is null");
        }
        ArrayList<ActionButton> actionButtons = player.getActionButtons();
        for (int i = 0; i < actionButtons.size(); i++) {
            if (actionButtons.get(i).equals(actionButton)) {
                actionButtons.remove(i);
                Object current = null;
                MySqlConnection dbConnection = MySqlConnection.getInstance();
                try {
                    current = dbConnection.getCharPool().borrowObject();
                    Connection connection = (Connection) current;
                    PreparedStatement statment;
                    statment = connection.prepareStatement(Query.deleteActionButton);
                    statment.setLong(1, player.getGuid());
                    statment.setInt(2, actionButton.getAction());
                    statment.setInt(3, actionButton.getSlotId());
                    statment.setInt(4, actionButton.getSpecGroup());
                    statment.execute();
                } finally {
                    if (current != null) {
                        dbConnection.getCharPool().returnObject(current);
                    }
                }
                return;
            }
        }
    }
    
    public static void removeActionButton(Player player, ActionButton actionButton) throws Exception {
        removeActionButton(player, actionButton, false);
    }
    
    public static int getActionInSlot(Player player, int slotId) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<ActionButton> actionButtons = player.getActionButtons();
        ArrayList<ActionButton> actionButtonsInSlot = new ArrayList<>();
        for (int i = 0; i < actionButtons.size(); i++) {
            if (actionButtons.get(i).getSlotId() == slotId) {
                return actionButtons.get(i).getAction();
            }
        }
        return 0;
    }
    
    public static ActionButton getActionButtonInSlot(Player player, int slotId) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<ActionButton> actionButtons = player.getActionButtons();
        ArrayList<ActionButton> actionButtonsInSlot = new ArrayList<>();
        for (int i = 0; i < actionButtons.size(); i++) {
            if (actionButtons.get(i).getSlotId() == slotId) {
                return actionButtons.get(i);
            }
        }
        return null;
    }
}
