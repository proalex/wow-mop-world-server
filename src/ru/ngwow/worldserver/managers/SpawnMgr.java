package ru.ngwow.worldserver.managers;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Creature;
import ru.ngwow.worldserver.entities.GameObject;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.WorldObject;
import ru.ngwow.worldserver.objectdefines.Vector4;

public class SpawnMgr {
    
    private static Vector<Creature> creatures = new Vector<>();
    private static Vector<GameObject> gameObjects = new Vector<>();
    private static final Logger log = Logger.getLogger(SpawnMgr.class.getName());
    
    public static ArrayList<WorldObject> getInRangeCreatures(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<WorldObject> inRangeCreatures = new ArrayList<>();
        for (int i = 0; i < creatures.size(); i++) {
            Creature current = creatures.get(i);
            if (!player.getInRangeObjects().contains(current) &&
                    player.checkDistance(current)) {
                inRangeCreatures.add(current);
            }
        }
        return inRangeCreatures;
    }
    
    public static ArrayList<WorldObject> getOutOfRangeCreatures(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<WorldObject> outOfRanceCreatures = new ArrayList<>();
        for (int i = 0; i < creatures.size(); i++) {
            Creature current = creatures.get(i);
            if (player.getInRangeObjects().contains(current) &&
                    !player.checkDistance(current)) {
                outOfRanceCreatures.add(current);
            }
        }
        return outOfRanceCreatures;
    }
    
    public static void loadCreatureSpawns() throws Exception {
        ResultSet result;
        Object current = null;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getWorldPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCreatureSpawns);
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getWorldPool().returnObject(current);
            }
        }
        while (result.next()) {
            Creature creature = new Creature(
                    result.getInt("id"),
                    result.getLong("guid"),
                    result.getInt("map"),
                    new Vector4(
                        result.getInt("x"),
                        result.getInt("y"),
                        result.getInt("z"),
                        result.getInt("o")));
            creatures.add(creature);
        }
        log.log(Level.INFO, "Loaded {0} creature spawns.", creatures.size());
    }
}
