package ru.ngwow.worldserver.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.Skill;

public class SkillMgr {
    
    private SkillMgr() {
    }
    
    public static void loadSkills(Player player) throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Object current = null;
        ResultSet result;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacterSkills);
            statment.setLong(1, player.getGuid());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (!result.next()) {
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharacterCreationSkills);
                statment.setInt(1, player.getRace());
                result = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
            while (result.next()) {
                addSkill(player, result.getInt("skill"), 0, true);
            }
        } else {
            do {
                addSkill(player, result.getInt("skill"), 0, false);
            } while (result.next());
        }
    }
    
    public static void addSkill(Player player, int skillId, int skillLevel, boolean addToDB)
            throws Exception{
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Skill skill = new Skill(skillId, skillLevel);
        player.getSkills().add(skill);
        if (addToDB) {
            Object current = null;
            MySqlConnection dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.addSkill);
                statment.setLong(1, player.getGuid());
                statment.setInt(2, skillId);
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
        }
    }
}
