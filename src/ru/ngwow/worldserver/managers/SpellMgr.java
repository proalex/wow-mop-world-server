package ru.ngwow.worldserver.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.Spell;

public class SpellMgr {
    
    private SpellMgr() {
    }
    
    public static void loadSpells(Player player) throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Object current = null;
        ResultSet result;
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacterSpells);
            statment.setLong(1, player.getGuid());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        if (!result.next()) {
            dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.getCharacterCreationSpells);
                statment.setInt(1, player.getRace());
                statment.setInt(2, player.getClassid());
                result = statment.executeQuery();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
            while (result.next()) {
                addSpell(player, result.getInt("spellId"), true);
            }
        } else {
            do {
                addSpell(player, result.getInt("spellId"), false);
            } while (result.next());
        }
    }
        
    public static void addSpell(Player player, int spellId, boolean saveToDB)
            throws Exception{
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Spell spell = new Spell(spellId);
        player.getSpells().add(spell);
        if (saveToDB) {
            Object current = null;
            MySqlConnection dbConnection = MySqlConnection.getInstance();
            try {
                current = dbConnection.getCharPool().borrowObject();
                Connection connection = (Connection) current;
                PreparedStatement statment;
                statment = connection.prepareStatement(Query.addSpell);
                statment.setLong(1, player.getGuid());
                statment.setLong(2, spellId);
                statment.execute();
            } finally {
                if (current != null) {
                    dbConnection.getCharPool().returnObject(current);
                }
            }
        }
    }
}
