package ru.ngwow.worldserver.managers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jboss.netty.channel.Channel;
import ru.ngwow.worldserver.constants.AccountDataMasks;
import ru.ngwow.worldserver.constants.MovementSpeed;
import ru.ngwow.worldserver.constants.ObjectType;
import ru.ngwow.worldserver.constants.UpdateFlag;
import ru.ngwow.worldserver.constants.UpdateType;
import ru.ngwow.worldserver.entities.Creature;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.entities.WorldObject;
import ru.ngwow.worldserver.handlers.ObjectHandler;
import ru.ngwow.worldserver.objectdefines.ObjectMovementValues;
import ru.ngwow.worldserver.objectdefines.Vector4;
import ru.ngwow.worldserver.opcodes.ServerOpcode;
import ru.ngwow.worldserver.packet.Packet;
import ru.ngwow.worldserver.run.Run;
import ru.ngwow.worldserver.session.WorldSession;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.bits.BitPack;
import ru.ngwow.worldserver.utils.parallel.Parallel;

public class WorldMgr {
    
    private static Vector<WorldSession> sessions = new Vector<>();
    private static final Logger log = Logger.getLogger(WorldMgr.class.getName());
    
    private WorldMgr() {
    }
    
    public static void worldUpdate() throws Exception {
        long saveCharactersTimer = System.currentTimeMillis();
        long updateVisibilityRangesTimer = System.currentTimeMillis();
        while (Run.running) {
            long currTime = System.currentTimeMillis();
            if ((currTime - updateVisibilityRangesTimer) >= WorldSettings.getInt("timer.updatevisibilityranges")) {
                updateVisibilityRangesTimer = currTime;
                Parallel.For(sessions, new Parallel.Operation<WorldSession>() {
                    @Override
                    public void perform(WorldSession session) {
                        try {
                            Player player = session.getPlayer();
                            WorldMgr.writeInRangeObjects(SpawnMgr.getInRangeCreatures(player), session, ObjectType.UNIT);
                            WorldMgr.writeInRangeObjects(getInRangeCharacter(player), session, ObjectType.PLAYER);
                            WorldMgr.writeOutOfRangeObjects(SpawnMgr.getOutOfRangeCreatures(player), session);
                            WorldMgr.writeOutOfRangeObjects(getOutOfRangeCharacter(player), session);
                        } catch (Exception ex) {
                            log.log(Level.WARNING, "{0} Exception in {1} detected: {2}.", new Object[]{
                                    new Date(), WorldMgr.class.getName(), ex.getMessage()});
                        }
                    };
                });
            }
            if ((currTime - saveCharactersTimer) >= WorldSettings.getInt("timer.savecharacters")) {
                saveCharactersTimer = currTime;
                Parallel.For(sessions, new Parallel.Operation<WorldSession>() {
                    @Override
                    public void perform(WorldSession session) {
                        try {
                            Player player = session.getPlayer();
                            player.savePositionToDB();
                        } catch (Exception ex) {
                            log.log(Level.WARNING, "{0} Exception in {1} detected: {2}.", new Object[]{
                                    new Date(), WorldMgr.class.getName(), ex.getMessage()});
                        }
                    };
                });
            }
            Thread.sleep(1);
        }
    }
    
    public static boolean addSession(WorldSession session) {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Player player = session.getPlayer();
        long guid = player.getGuid();
        for (int i = 0; i < sessions.size(); i++) {
            if (sessions.get(i).equals(session)) {
                return false;
            }
        }
        sessions.add(session);
        return true;
    }
    
    public static void removeSession(WorldSession session) throws Exception {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        for (int i = 0; i < sessions.size(); i++) {
            if (sessions.get(i).equals(session)) {
                Player player = session.getPlayer();
                sendToInRangeCharacter(player, ObjectHandler.destroyObject(player));
                sessions.remove(i);
                player.setInGame(false);
                player.savePositionToDB();
                break;
            }
        }
    }
    
    public static void writeAccountDataTimes(AccountDataMasks mask, WorldSession session)
            throws IOException {
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (mask == null) {
            throw new NullPointerException("mask is null");
        }
        Channel channel = session.getChannel();
        Packet output = new Packet(ServerOpcode.ACCOUNT_DATA_TIMES);
        BitPack bout = new BitPack(output);
        output.writeUnixTime();
        for (int i = 0; i < 8; i++) {
            output.writeUInt32(0);
        }
        output.writeUInt32(mask.getData());
        bout.write(0);
        bout.flush();
        channel.write(output);
    }
    
    public static void writeUpdateObjectMovement(Packet packet, WorldObject object,
            int updateFlags) throws IOException {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (packet == null) {
            throw new NullPointerException("packet is null");
        }
        ObjectMovementValues values = new ObjectMovementValues(updateFlags);
        Vector4 position = object.getPosition();
        BitPack bout = new BitPack(packet);
        bout.setGuid(object.getGuid());
        bout.write(0);
        bout.write(0, 22);
        bout.write(values.isVehicle);
        bout.write(0);
        bout.write(values.hasGoTransportPosition);
        bout.write(0);
        bout.write(0);
        bout.write(0);
        bout.write(0);
        bout.write(0);
        bout.write(false);
        bout.write(values.hasTarget);
        bout.write(0);
        bout.write(0);
        bout.write(values.isSelf);
        bout.write(0);
        bout.write(values.isAlive);
        bout.write(0);
        bout.write(0);
        bout.write(values.hasAnimKits);
        bout.write(values.hasStationaryPosition);
        if (values.isAlive) {
            bout.writeGuidMask(0);
            bout.write(1);
            bout.writeGuidMask(4, 7);
            bout.write(1);
            bout.writeGuidMask(5, 2);
            bout.write(0);
            bout.write(1);
            bout.write(0);
            bout.write(0);
            bout.write(0);
            bout.write(!values.hasRotation);
            bout.write(values.isTransport);
            bout.write(1);
            bout.writeGuidMask(6);
            bout.write(0, 19);
            bout.writeGuidMask(1);
            bout.write(1);
            bout.writeGuidMask(3);
            bout.write(0, 22);
            bout.write(0);
            bout.write(0);
        }
        bout.flush();
        if (values.isAlive)
        {
            packet.writeUInt32(0);
            bout.writeGuidBytes(2, 1);
            packet.writeFloat((float)MovementSpeed.walkSpeed);
            packet.writeFloat((float)MovementSpeed.runSpeed);
            bout.writeGuidBytes(0, 3);
            packet.writeFloat((float)MovementSpeed.swimBackSpeed);
            packet.writeFloat((float)MovementSpeed.turnSpeed);
            bout.writeGuidBytes(5);
            packet.writeFloat(position.z);
            packet.writeFloat(position.o);
            bout.writeGuidBytes(6);
            packet.writeFloat((float)MovementSpeed.pitchSpeed);
            packet.writeFloat((float)MovementSpeed.runBackSpeed);
            packet.writeFloat(position.y);
            packet.writeFloat((float)MovementSpeed.swimSpeed);
            packet.writeFloat((float)MovementSpeed.flyBackSpeed);
            bout.writeGuidBytes(7);
            packet.writeFloat((float)MovementSpeed.flySpeed);
            packet.writeFloat(position.x);
            bout.writeGuidBytes(4);
        }
        if (values.hasStationaryPosition)
        {
            packet.writeFloat(position.x);
            packet.writeFloat(position.z);
            packet.writeFloat(position.o);
            packet.writeFloat(position.y);
        }
    }
    
    public static WorldSession getSession(long guid) {
        for (int i = 0; i < sessions.size(); i++) {
            Player player = sessions.get(i).getPlayer();
            if (player.getGuid() == guid) {
                return sessions.get(i);
            }
        }
        return null;
    }
    
    public static WorldSession getSession(String name) {
        for (int i = 0; i < sessions.size(); i++) {
            Player player = sessions.get(i).getPlayer();
            if (player.getName().compareToIgnoreCase(name) == 0) {
                return sessions.get(i);
            }
        }
        return null;
    }
    
    public static void sendToInRangeCharacter(Player player, Packet packet,
            boolean withCharacter) {
        if (packet == null) {
            throw new NullPointerException("packet is null");
        }
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        packet.markReaderIndex();
        for (int i = 0; i < sessions.size(); i++) {
            WorldSession session = sessions.get(i);
            Player current = session.getPlayer();
            if (player.getInRangeObjects().contains(current) &&
                    current.getInRangeObjects().contains(player)) {
                Channel channel = session.getChannel();
                channel.write(packet);
                packet.resetReaderIndex();
            }
        }
        if (withCharacter) {
            Channel channel = player.getSession().getChannel();
            channel.write(packet);
        }
    }
    
    public static void sendToInRangeCharacter(Player player, Packet packet) {
        sendToInRangeCharacter(player, packet, false);
    }
    
    public static ArrayList<WorldObject> getInRangeCharacter(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<WorldObject> inRangeCharacters = new ArrayList<>();
        for (int i = 0; i < sessions.size(); i++) {
            Player current = sessions.get(i).getPlayer();
            if (!player.equals(current) &&
                    current.isInGame() &&
                    !player.getInRangeObjects().contains(current) &&
                    player.checkDistance(current)) {
                inRangeCharacters.add(current);
            }
        }
        return inRangeCharacters;
    }
    
    public static ArrayList<WorldObject> getOutOfRangeCharacter(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<WorldObject> outOfRanceCharacters = new ArrayList<>();
        for (int i = 0; i < sessions.size(); i++) {
            Player current = sessions.get(i).getPlayer();
            if (player.getInRangeObjects().contains(current) && 
                    (!current.isInGame() || !player.checkDistance(current))) {
                outOfRanceCharacters.add(current);
            }
        }
        return outOfRanceCharacters;
    }
    
    public static void writeInRangeObjects(ArrayList<WorldObject> objects,
            WorldSession session, ObjectType type) throws IOException {
        if (objects == null) {
            throw new NullPointerException("objects is null");
        }
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        if (type == null) {
            throw new NullPointerException("type is null");
        }
        Player player = session.getPlayer();
        Channel channel = session.getChannel();
        int size = objects.size();
        int updateFlags = UpdateFlag.ROTATION.getData();
        if (size > 0) {
            updateFlags |= type == ObjectType.GAMEOBJECT ? 
                    UpdateFlag.STATIONARY_POSITION.getData() : UpdateFlag.ALIVE.getData();
            for (int i = 0; i < size; i++) {
                Packet output = new Packet(ServerOpcode.OBJECT_UPDATE);
                output.writeUInt16(player.getMap());
                output.writeUInt32(1);
                WorldObject object = objects.get(i);
                if (!player.getInRangeObjects().contains(object)) {
                    writeCreateObject(output, object, updateFlags, type);
                    if (!player.equals(object)) {
                        player.getInRangeObjects().add(object);
                    }
                }
                channel.write(output);
            }
        }
    }
    
    public static void writeOutOfRangeObjects(ArrayList<WorldObject> objects, WorldSession session) {
        if (objects == null) {
            throw new NullPointerException("objects is null");
        }
        if (session == null) {
            throw new NullPointerException("session is null");
        }
        Player player = session.getPlayer();
        Channel channel = session.getChannel();
        int size = objects.size();
        int updateFlags = UpdateFlag.ROTATION.getData();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Packet output = new Packet(ServerOpcode.OBJECT_UPDATE);
                output.writeUInt16(player.getMap());
                output.writeUInt32(1);
                output.writeUInt8(UpdateType.OUT_OF_RANGE.getData());
                output.writeUInt32(1);
                WorldObject object = objects.get(i);
                output.writeUInt32(object.getGuid());
                player.getInRangeObjects().remove(object);
                channel.write(output);
            }
        }
    }
    
    private static void writeCreateObject(Packet output, WorldObject object,
            int updateFlags, ObjectType type) throws IOException {
        if (output == null) {
            throw new NullPointerException("output is null");
        }
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (type == null) {
            throw new NullPointerException("type is null");
        }
        output.writeUInt8(UpdateType.CREATE_OBJECT.getData());
        output.writeGuid(object.getGuid());
        output.writeUInt8(type.getData());
        writeUpdateObjectMovement(output, object, updateFlags);
        object.setUpdateFields();
        object.writeUpdateFields(output);
        object.writeDynamicUppdateFields(output);
    }
}
