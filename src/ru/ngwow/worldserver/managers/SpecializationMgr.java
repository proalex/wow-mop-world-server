package ru.ngwow.worldserver.managers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import ru.ngwow.worldserver.clientdb.query.DBCQuery;
import ru.ngwow.worldserver.clientdb.structures.DBCSpecializationSpells;
import ru.ngwow.worldserver.clientdb.structures.DBCSpellLevels;
import ru.ngwow.worldserver.clientdb.structures.DBCTalent;
import ru.ngwow.worldserver.constants.ClassId;
import ru.ngwow.worldserver.database.MySqlConnection;
import ru.ngwow.worldserver.database.query.Query;
import ru.ngwow.worldserver.entities.Player;
import ru.ngwow.worldserver.objectdefines.Talent;
import ru.ngwow.worldserver.utils.uint.UInt32;

public class SpecializationMgr {
    
    public static void loadTalents(Player player) throws Exception {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        Object current = null;
        ResultSet result;
        ArrayList<Talent> talents = new ArrayList<>();
        MySqlConnection dbConnection = MySqlConnection.getInstance();
        try {
            current = dbConnection.getCharPool().borrowObject();
            Connection connection = (Connection) current;
            PreparedStatement statment;
            statment = connection.prepareStatement(Query.getCharacterTalents);
            statment.setLong(1, player.getGuid());
            result = statment.executeQuery();
        } finally {
            if (current != null) {
                dbConnection.getCharPool().returnObject(current);
            }
        }
        while (result.next()) {
            addTalent(player, new Talent(result.getInt("talentId"), result.getInt("specGroup")));
        }
    }
    
    public static void addTalent(Player player, Talent talent) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        if (talent == null) {
            throw new NullPointerException("talent is null");
        }
        player.getTalents().add(talent);
    }
    
    public static ArrayList<Talent> getTalentsBySpecGroup(Player player, int specGroup) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<Talent> result = new ArrayList<>();
        ArrayList<Talent> talents = player.getTalents();
        for (int i = 0; i < talents.size(); i++) {
            if (talents.get(i).getSpecGroup() == specGroup) {
                result.add(talents.get(i));
            }
        }
        return result;
    }
    
    public static ArrayList<DBCTalent> getTalentSpells(Player player, int specGroup) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        if (getSpentTalentRowCount(player, specGroup) == 0) {
            return null;
        }
        ArrayList<Talent> talents = getTalentsBySpecGroup(player, specGroup);
        return DBCQuery.getTalentSpells(talents);
    }
    
    public static ArrayList<DBCSpecializationSpells> getSpecializationSpells(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        if (player.getActiveSpecId() == 0) {
            return null;
        }
        ArrayList<DBCSpecializationSpells> knownSpecSpells = new ArrayList<>();
        ArrayList<DBCSpecializationSpells> specSpells =
                DBCQuery.getSpecializationSpells(player.getActiveSpecId());
        for (int i = 0; i < specSpells.size(); i++) {
            UInt32 spellLevelId = DBCQuery.getSpellLvlId(specSpells.get(i).spell.longValue());
            if (spellLevelId == null) {
                continue;
            }
            DBCSpellLevels spellLevel = DBCQuery.getSpellLevel(spellLevelId.longValue());
            long baseLevel = 0L;
            if (spellLevel != null) {
                baseLevel = spellLevel.baseLevel.longValue();
            }
            if (player.getLevel() >= baseLevel) {
                knownSpecSpells.add(specSpells.get(i));
            }
        }
        return knownSpecSpells;
    }
    
    public static int getSpentTalentRowCount(Player player, int specGroup) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        ArrayList<Talent> talents = player.getTalents();
        int count = 0;
        for (int i = 0; i < talents.size(); i++) {
            if (talents.get(i).getSpecGroup() == specGroup) {
                count++;
            }
        }
        return count;
    }
    
    public static int getUnspentTalentRowCount(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        int spentTalentRows = getSpentTalentRowCount(player, player.getActiveSpecGroup());
        int maxTalentRows = getMaxTalentRowCount(player);
        return maxTalentRows - spentTalentRows;
    }
    
    public static int getMaxTalentRowCount(Player player) {
        if (player == null) {
            throw new NullPointerException("player is null");
        }
        int talentPoints = 0;
        byte[] talentLevels;
        
        if (player.getClassid() != ClassId.DEATHKNIGHT.getData()) {
            talentLevels = new byte[] {15, 30, 45, 60, 75, 90};
        } else {
            talentLevels = new byte[] {56, 57, 58, 60, 75, 90};
        }
        for (int i = 0; i < talentLevels.length; i++) {
            if (player.getLevel() >= talentLevels[i]) {
                talentPoints++;
            }
        }
        return talentPoints;
    }
}
