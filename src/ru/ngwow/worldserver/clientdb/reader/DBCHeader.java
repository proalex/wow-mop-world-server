package ru.ngwow.worldserver.clientdb.reader;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCHeader {
    
    private String signature;
    private UInt32 recordCount;
    private UInt32 fieldCount;
    private UInt32 recordSize;
    private UInt32 stringBlockSize;

    public DBCHeader(String signature, UInt32 recordCount, UInt32 fieldCount,
            UInt32 recordSize, UInt32 stringBlockSize) {
        if (signature == null) {
            throw new NullPointerException("signature is null");
        }
        if (recordCount == null) {
            throw new NullPointerException("recordCount is null");
        }
        if (fieldCount == null) {
            throw new NullPointerException("fieldCount is null");
        }
        if (recordSize == null) {
            throw new NullPointerException("recordSize is null");
        }
        if (stringBlockSize == null) {
            throw new NullPointerException("stringBlockSize is null");
        }
        this.signature = signature;
        this.recordCount = recordCount;
        this.fieldCount = fieldCount;
        this.recordSize = recordSize;
        this.stringBlockSize = stringBlockSize;
    }
    
    public String getSignature() {
        return signature;
    }

    public long getRecordCount() {
        return recordCount.longValue();
    }

    public long getFieldCount() {
        return fieldCount.longValue();
    }

    public long getRecordSize() {
        return recordSize.longValue();
    }

    public long getStringBlockSize() {
        return stringBlockSize.longValue();
    }

    public boolean isIsValidDbcFile() {
        return signature.compareTo("WDBC") == 0;
    }
    public boolean isIsValidDb2File() {
        return signature.compareTo("WDB2") == 0;
    }
}
