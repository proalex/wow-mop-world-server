package ru.ngwow.worldserver.clientdb.reader;

import java.lang.reflect.Field;
import java.util.ArrayList;
import ru.ngwow.worldserver.exceptions.DBCInvalidGameBuildException;
import ru.ngwow.worldserver.settings.WorldSettings;
import ru.ngwow.worldserver.utils.uint.UInt32;
import ru.ngwow.worldserver.utils.variabletype.VariableType;

public class DBCReader<T> {
    
    public ArrayList<T> read(String dbcFile, Class<T> object) throws Exception {
        if (dbcFile == null) {
            throw new NullPointerException("dbcFile is null");
        }
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        ArrayList<T> result = new ArrayList<>();
        DBCFile file = new DBCFile(WorldSettings.get("path.dbc") + dbcFile, "r");
        DBCHeader header = new DBCHeader(file.readString(4), file.readUInt32(),
                file.readUInt32(), file.readUInt32(), file.readUInt32());
        if (header.isIsValidDb2File()) {
            UInt32 hash = file.readUInt32();
            UInt32 wowBuild = file.readUInt32();
            if (wowBuild.longValue() != 16826) {
                throw new DBCInvalidGameBuildException("invalid dbc file " + dbcFile);
            }
            UInt32 unknown = file.readUInt32();
            int min = file.readInt32();
            int max = file.readInt32();
            int locale = file.readInt32();
            int unknown2 = file.readInt32();
            if(max != 0) {
                int diff = (max - min) + 1;
                file.skipBytes(diff * 6);
            }
        }
        if (header.isIsValidDb2File() || header.isIsValidDbcFile()) {
            Field[] fields = object.getFields();
            long lastStringOffset = 0;
            for (int i = 0; i < header.getRecordCount(); i++) {
                T current = object.newInstance();
                for (Field field : fields) {
                    VariableType type = VariableType.getType(field.getType().getCanonicalName());
                    switch (type) {
                        case STRING:
                            UInt32 stringOffset = file.readUInt32();
                            if (stringOffset.longValue() != lastStringOffset) {
                                long currentPos = file.getFilePointer();
                                long stringStart = (header.getRecordCount() * header.getRecordSize())
                                        + 20 + stringOffset.longValue();
                                file.seek(stringStart);
                                field.set(current, file.readCString());
                                file.seek(currentPos);
                            }
                            break;
                        case UINT32:
                            field.set(current, file.readUInt32());
                            break;
                        case FLOAT:
                            field.set(current, new Float(file.readFloat()));
                            break;
                        case FLOAT_ARRAY:
                            file.skipBytes(24);
                            break;
                    }
                }
                result.add(current);
            }
        } else {
            throw new DBCInvalidGameBuildException("invalid dbc file " + dbcFile);
        }
        return result;
    }
}
