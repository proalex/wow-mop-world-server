package ru.ngwow.worldserver.clientdb.reader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.charset.Charset;
import ru.ngwow.worldserver.utils.uint.UInt16;
import ru.ngwow.worldserver.utils.uint.UInt32;
import ru.ngwow.worldserver.utils.uint.UInt64;
import ru.ngwow.worldserver.utils.uint.UInt8;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;
import ru.ngwow.worldserver.utils.endian.Endian;

public class DBCFile extends RandomAccessFile {
    
    public DBCFile(String name, String mode) throws FileNotFoundException {
        super(name, mode);
    }
    
    public UInt8 readUInt8() throws IOException {
        return uint8(readByte());
    }
    
    public UInt16 readUInt16() throws IOException {
        return uint16(Endian.swapEndian(readShort()));
    }
    
    public UInt32 readUInt32() throws IOException {
        return uint32(Endian.swapEndian(readInt()));
    }
    
    public byte readInt8() throws IOException {
        return readByte();
    }
    
    public short readInt16() throws IOException {
        return Endian.swapEndian(readShort());
    }
    
    public int readInt32() throws IOException {
        return Endian.swapEndian(readInt());
    }
    
    public String readString(int size) throws IOException {
        byte[] data = new byte[size];
        read(data);
        return new String(data, Charset.forName("UTF-8"));
    }
    
    public String readCString() throws IOException {
        byte[] data = new byte[128];
        int i = 0;
        while ((data[i++] = readByte()) != (byte) 0x00);
        return new String(data, Charset.forName("UTF-8"));
    }
}
