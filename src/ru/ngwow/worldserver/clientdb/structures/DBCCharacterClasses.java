package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCCharacterClasses {
    
    public UInt32 id;
    public UInt32 displayPower;
    public UInt32 petNameToken;
    public UInt32 name;
    public UInt32 unknown;
    public UInt32 unknown2;
    public UInt32 clientFileName;
    public UInt32 spellClassSet;
    public UInt32 unknown3;
    public UInt32 cinematicSequence;
    public UInt32 unknown4;
    public UInt32 unknown5;
    public UInt32 unknown6;
    public UInt32 unknown7;
    public UInt32 unknown8;
    public UInt32 unknown9;
    public UInt32 unknown10;
    public UInt32 unknown11;
}
