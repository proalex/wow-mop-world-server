package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCTalent {
    
    public UInt32 id;
    public UInt32 unknown;
    public UInt32 row;
    public UInt32 column;
    public UInt32 spellId;
    public UInt32 unknown2;
    public UInt32 unknown3;
    public UInt32 unknown4;
    public UInt32 classId;
    public UInt32 replaceSpellId;
    public UInt32 unknown5;
}
