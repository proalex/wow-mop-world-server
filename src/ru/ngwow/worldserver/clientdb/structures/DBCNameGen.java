package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCNameGen {
    
    public UInt32 id;
    public String name;
    public UInt32 race;
    public UInt32 gender;
}
