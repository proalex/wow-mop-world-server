package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCCharacterRaces {
    
    public UInt32 id;
    public UInt32 flags;
    public UInt32 faction;
    public UInt32 explorationSound;
    public UInt32 maleDisplayId;
    public UInt32 femaleDisplayId;
    public UInt32 clientPrefix;
    public UInt32 naseLanguage; 
    public UInt32 creatureType;
    public UInt32 resSicknessSpellID;
    public UInt32 splashSoundID;
    public UInt32 clientFileName; 
    public UInt32 cinematicSequence;
    public UInt32 baseFaction;
    public UInt32 name;
    public UInt32 unknown;
    public UInt32 unknown2;
    public UInt32 maleFacialHairs;
    public UInt32 femaleFacialHairs;
    public UInt32 hairs;
    public UInt32 otherFactionRace;
    public UInt32 unknown3;
    public UInt32 unknown4;
    public UInt32 unknown5;
    public UInt32 raceCount;
    public UInt32 unknown6;
    public UInt32 unknown7;
    public UInt32 unknown8;
    public Float[] unknown9 = new Float[6];
    public UInt32 unknown10;
    public UInt32 unknown11;
}
