package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCCharacterSpecialization {
    
    public UInt32 id;
    public UInt32 icon;
    public UInt32 classId;
    public UInt32 masterySpellId;
    public UInt32 unknown;
    public UInt32 tabId;
    public UInt32 unknown2;
    public UInt32 roleTypeId;
    public UInt32 unknown3;
    public UInt32 unknown4;
    public UInt32 unknown5;
    public UInt32 name;
    public UInt32 description;
    public UInt32 unknown6;
}
