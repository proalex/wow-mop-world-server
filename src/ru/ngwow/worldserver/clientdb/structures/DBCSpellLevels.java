package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCSpellLevels {
    
    public UInt32 id;
    public UInt32 spellId; 
    public UInt32 unknown;
    public UInt32 baseLevel;
    public UInt32 maxLevel;
    public UInt32 spellLevel;
}
