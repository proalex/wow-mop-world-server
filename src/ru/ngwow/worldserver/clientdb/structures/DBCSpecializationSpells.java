package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCSpecializationSpells {
    
    public UInt32 id;
    public UInt32 group;
    public UInt32 spell;
    public UInt32 spell2;
    public UInt32 unknown;
}
