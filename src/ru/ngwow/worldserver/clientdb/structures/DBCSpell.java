package ru.ngwow.worldserver.clientdb.structures;

import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCSpell {
    
    public UInt32 id;
    public UInt32 name;
    public UInt32 subText;
    public UInt32 description;
    public UInt32 auraDescription;
    public UInt32 runeCostId;
    public UInt32 spellMissileId;
    public UInt32 spellDescriptionVariableId;
    public Float unknown;
    public UInt32 spellScalingId;
    public UInt32 spellAuraOptionsId;
    public UInt32 spellAuraRestrictionsId;
    public UInt32 spellCastingRequirementsId;
    public UInt32 spellCategoriesId;
    public UInt32 spellClassOptionsId;
    public UInt32 spellCooldownsId;
    public UInt32 spellEquippedItemsId;
    public UInt32 spellInterruptsId;
    public UInt32 spellLevelsId;
    public UInt32 spellReagentsId;
    public UInt32 spellShapeshiftId;
    public UInt32 spellTargetRestrictionsId;
    public UInt32 spellTotemsId;
    public UInt32 researchProjectId;
    public UInt32 spellMiscId;
}
