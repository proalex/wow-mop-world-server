package ru.ngwow.worldserver.clientdb.query;

import java.util.ArrayList;
import ru.ngwow.worldserver.clientdb.loader.DBCLoader;
import ru.ngwow.worldserver.clientdb.structures.DBCCharacterRaces;
import ru.ngwow.worldserver.clientdb.structures.DBCSpecializationSpells;
import ru.ngwow.worldserver.clientdb.structures.DBCSpell;
import ru.ngwow.worldserver.clientdb.structures.DBCSpellLevels;
import ru.ngwow.worldserver.clientdb.structures.DBCTalent;
import ru.ngwow.worldserver.objectdefines.Talent;
import ru.ngwow.worldserver.utils.uint.UInt32;

public class DBCQuery {
    
    private DBCQuery() {
        
    }
    
    public static ArrayList<DBCTalent> getTalentSpells(ArrayList<Talent> talents) {
        if (talents == null) {
            throw new NullPointerException("talents is null");
        }
        ArrayList<DBCTalent> result = new ArrayList<>();
        ArrayList<DBCTalent> data = DBCLoader.getTalents();
        for (int i = 0; i < talents.size(); i++) {
            for (int j = 0; j < data.size(); j++) {
                if (talents.get(i).getId() == data.get(j).id.longValue()) {
                    result.add(data.get(j));
                    break;
                }
            }
        }
        return result;
    }
    
    public static ArrayList<DBCSpecializationSpells> getSpecializationSpells(int activeSpecId) {
        ArrayList<DBCSpecializationSpells> specSpells = new ArrayList<>();
        ArrayList<DBCSpecializationSpells> data = DBCLoader.getSpecializationSpells();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).group.longValue() == activeSpecId) {
                specSpells.add(data.get(i));
            }
        }
        return specSpells;
    }
    
    public static UInt32 getSpellLvlId(long id) {
        ArrayList<DBCSpell> data = DBCLoader.getSpells();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.longValue() == id) {
                return data.get(i).spellLevelsId;
            }
        }
        return null;
    }
    
    public static DBCSpellLevels getSpellLevel(long id) {
        ArrayList<DBCSpellLevels> data = DBCLoader.getSpellLevels();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.longValue() == id) {
                return data.get(i);
            }
        }
        return null;
    }
    
    public static UInt32 getCinematicSequence(int race) {
        ArrayList<DBCCharacterRaces> data = DBCLoader.getCharacterRaces();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.longValue() == race) {
                return data.get(i).cinematicSequence;
            }
        }
        return null;
    }
    
    public static UInt32 getFaction(int race) {
        ArrayList<DBCCharacterRaces> data = DBCLoader.getCharacterRaces();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.longValue() == race) {
                return data.get(i).faction;
            }
        }
        return null;
    }
    
    public static UInt32 getDisplayId(int race, int gender) {
        ArrayList<DBCCharacterRaces> data = DBCLoader.getCharacterRaces();
        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).id.longValue() == race) {
                return gender == 0 ? data.get(i).maleDisplayId : data.get(i).femaleDisplayId;
            }
        }
        return null;
    }
}
