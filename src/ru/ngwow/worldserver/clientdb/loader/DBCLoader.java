package ru.ngwow.worldserver.clientdb.loader;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import ru.ngwow.worldserver.clientdb.reader.DBCReader;
import ru.ngwow.worldserver.clientdb.structures.*;

public class DBCLoader {
    
    private static ArrayList<DBCCharacterClasses> characterClasses;
    private static ArrayList<DBCCharacterRaces> characterRaces;
    private static ArrayList<DBCCharacterSpecialization> characterSpecialization;
    private static ArrayList<DBCNameGen> nameGen;
    private static ArrayList<DBCSpecializationSpells> specializationSpells;
    private static ArrayList<DBCSpell> spells;
    private static ArrayList<DBCSpellLevels> spellLevels;
    private static ArrayList<DBCTalent> talents;
    private static final Logger log = Logger.getLogger(DBCLoader.class.getName());

    private DBCLoader() {
    }
    
    public static void load() throws Exception {
        log.log(Level.INFO, "Loading client database...");
        characterClasses = new DBCReader<DBCCharacterClasses>().read("ChrClasses.dbc", DBCCharacterClasses.class);
        characterRaces = new DBCReader<DBCCharacterRaces>().read("ChrRaces.dbc", DBCCharacterRaces.class);
        characterSpecialization = new DBCReader<DBCCharacterSpecialization>().read("ChrSpecialization.dbc", DBCCharacterSpecialization.class);
        nameGen = new DBCReader<DBCNameGen>().read("NameGen.dbc", DBCNameGen.class);
        specializationSpells = new DBCReader<DBCSpecializationSpells>().read("SpecializationSpells.dbc", DBCSpecializationSpells.class);
        spells = new DBCReader<DBCSpell>().read("Spell.dbc", DBCSpell.class);
        spellLevels = new DBCReader<DBCSpellLevels>().read("SpellLevels.dbc", DBCSpellLevels.class);
        talents = new DBCReader<DBCTalent>().read("Talent.dbc", DBCTalent.class);
    }
    
    public static ArrayList<DBCCharacterClasses> getCharacterClasses() {
        if (characterClasses == null) {
            throw new NullPointerException("characterClasses is null");
        }
        return characterClasses;
    }

    public static ArrayList<DBCCharacterRaces> getCharacterRaces() {
        if (characterRaces == null) {
            throw new NullPointerException("characterRaces is null");
        }
        return characterRaces;
    }

    public static ArrayList<DBCCharacterSpecialization> getCharacterSpecialization() {
        if (characterSpecialization == null) {
            throw new NullPointerException("characterSpecialization is null");
        }
        return characterSpecialization;
    }

    public static ArrayList<DBCNameGen> getNameGen() {
        if (nameGen == null) {
            throw new NullPointerException("nameGen is null");
        }
        return nameGen;
    }

    public static ArrayList<DBCSpecializationSpells> getSpecializationSpells() {
        if (specializationSpells == null) {
            throw new NullPointerException("specializationSpells is null");
        }
        return specializationSpells;
    }

    public static ArrayList<DBCSpell> getSpells() {
        if (spells == null) {
            throw new NullPointerException("spells is null");
        }
        return spells;
    }

    public static ArrayList<DBCSpellLevels> getSpellLevels() {
        if (spellLevels == null) {
            throw new NullPointerException("spellLevels is null");
        }
        return spellLevels;
    }

    public static ArrayList<DBCTalent> getTalents() {
        if (talents == null) {
            throw new NullPointerException("talents is null");
        }
        return talents;
    }
}
