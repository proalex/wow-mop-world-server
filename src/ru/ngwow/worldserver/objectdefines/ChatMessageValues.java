package ru.ngwow.worldserver.objectdefines;

import ru.ngwow.worldserver.constants.MessageType;

public class ChatMessageValues {
    
    private boolean hasLanguage = false;
    private boolean hasRealmId = false;
    private MessageType type;
    private int language;
    private int realmId;
    private String message;
    
    public ChatMessageValues(MessageType type, String message) {
        if (type == null) {
            throw new NullPointerException("type is null");
        }
        if (message == null) {
            throw new NullPointerException("message is null");
        }
        this.type = type;
        this.message = message;
        this.hasLanguage = hasLanguage;
       
    }
    
    public void setRealmId(int realmId) {
         this.realmId = realmId;
         this.hasRealmId = true;
    }
    
    public void setLanguage(int language) {
        this.hasLanguage = true;
        this.language = language;
    }

    public boolean hasLanguage() {
        return hasLanguage;
    }

    public boolean hasRealmId() {
        return hasRealmId;
    }

    public MessageType getType() {
        return type;
    }

    public int getLanguage() {
        return language;
    }

    public int getRealmId() {
        return realmId;
    }

    public String getMessage() {
        return message;
    }
}
