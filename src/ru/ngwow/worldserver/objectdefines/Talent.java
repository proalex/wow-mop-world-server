package ru.ngwow.worldserver.objectdefines;

public class Talent {
    
    private int id;
    private int specGroup;
    
    public Talent(int id, int spedGroup) {
        this.id = id;
        this.specGroup = specGroup;
    }

    public int getId() {
        return id;
    }

    public int getSpecGroup() {
        return specGroup;
    }
}
