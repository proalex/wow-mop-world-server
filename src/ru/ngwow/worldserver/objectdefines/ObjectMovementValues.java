package ru.ngwow.worldserver.objectdefines;

import ru.ngwow.worldserver.constants.MovementFlag;
import ru.ngwow.worldserver.constants.MovementFlag2;
import ru.ngwow.worldserver.constants.UpdateFlag;
import ru.ngwow.worldserver.utils.uint.UInt32;

public class ObjectMovementValues {

    public boolean hasAnimKits;
    public boolean hasUnknown;
    public boolean hasUnknown2;
    public boolean isVehicle;
    public boolean hasUnknown3;
    public boolean hasStationaryPosition;
    public boolean hasGoTransportPosition;
    public boolean isSelf;
    public boolean isAlive;
    public boolean hasUnknown4;
    public boolean hasTarget;
    public boolean hasRotation;
    public boolean isTransport;
    public boolean hasMovementFlags;
    public boolean hasMovementFlags2;
    public boolean isInterpolated;
    public boolean isInterpolated2;
    public int movementFlags;
    public int movementFlags2;
    public UInt32 time;
    public boolean isFallingOrJumping;
    public boolean hasJumpData;
    public float jumpVelocity;
    public float currentSpeed;
    public float sin;
    public float cos;
    public UInt32 fallTime;

    public ObjectMovementValues(int updateFlags) {
        isSelf = (updateFlags & UpdateFlag.SELF.getData()) != 0;
        isAlive = (updateFlags & UpdateFlag.ALIVE.getData()) != 0;
        hasRotation = (updateFlags & UpdateFlag.ROTATION.getData()) != 0;
        hasStationaryPosition = (updateFlags & UpdateFlag.STATIONARY_POSITION.getData()) != 0;
        hasTarget = (updateFlags & UpdateFlag.TARGET.getData()) != 0;
        isTransport = (updateFlags & UpdateFlag.TRANSPORT.getData()) != 0;
        hasGoTransportPosition = (updateFlags & UpdateFlag.GO_TRANSPORT_POSITION.getData()) != 0;
        hasAnimKits = (updateFlags & UpdateFlag.ANIMKITS.getData()) != 0;
        isVehicle = (updateFlags & UpdateFlag.VEHICLE.getData()) != 0;
    }

    public ObjectMovementValues() {
    }
}
