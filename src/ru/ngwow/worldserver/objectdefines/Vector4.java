package ru.ngwow.worldserver.objectdefines;

public class Vector4 {
    
    public float x;
    public float y;
    public float z;
    public float o;
    
    public Vector4() {
    }
    
    public Vector4(float x, float y, float z, float o) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.o = o;
    }
    
    public float getDistance(Vector4 vector) {
        float disX = (float)Math.pow(x - vector.x, 2);
        float disY = (float)Math.pow(y - vector.y, 2);
        float disZ = (float)Math.pow(z - vector.z, 2);
        return disX + disY + disZ;
    }
}
