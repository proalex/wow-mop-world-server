package ru.ngwow.worldserver.objectdefines;

import ru.ngwow.worldserver.constants.HighGuidType;
import ru.ngwow.worldserver.exceptions.NoSuchGuidTypeException;
import ru.ngwow.worldserver.utils.uint.UInt64;
import static ru.ngwow.worldserver.utils.uint.Unsigned.*;

public class SmartGuid {
    
    private UInt64 guid;
    
    public SmartGuid(UInt64 low, int id, HighGuidType highType) {
        guid = uint64(low.longValue() | ((long) id << 32) | (long) highType.getData() << 52);
    }
    
    public UInt64 getGuid() {
        return guid;
    }
    
    public static HighGuidType getGuidType(UInt64 guid) throws NoSuchGuidTypeException {
        return HighGuidType.getGuidType(guid.longValue() >> 52);
    }
    
    public static int getId(UInt64 guid) {
        return (int) ((guid.longValue() >> 32) & 0xFFFFF);
    }
}
