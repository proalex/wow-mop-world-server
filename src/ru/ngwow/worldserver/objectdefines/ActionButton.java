package ru.ngwow.worldserver.objectdefines;

public class ActionButton {
    
    private int action;
    private int slotId;
    private int specGroup;
    
    public ActionButton(int action, int slotId, int specGroup) {
        this.action = action;
        this.slotId = slotId;
        this.specGroup = specGroup;
    }

    public int getAction() {
        return action;
    }

    public int getSlotId() {
        return slotId;
    }

    public int getSpecGroup() {
        return specGroup;
    }
    
    @Override
    public boolean equals(Object object) {
        if (object == null) {
            throw new NullPointerException("object is null");
        }
        if (!(object instanceof ActionButton)) {
            return false;
        }
        ActionButton actionButton = (ActionButton) object;
        if (actionButton.action == action && actionButton.slotId == slotId &&
                actionButton.specGroup == specGroup) {
            return true;
        } else {
            return false;
        }
    }
}
